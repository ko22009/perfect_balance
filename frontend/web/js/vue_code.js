var app = new Vue({
	el: '#app',
	data: {
		tarifs: {},
		current_day: (new Date()).getFullYear() + '-' + ("0" + ((new Date()).getMonth() + 1)).slice(-2) + '-' + ("0" + (new Date()).getDate()).slice(-2),
		tarifs_date: [],
		pages: [
			-1, 0, 1
		],
		basket: [],
		delivery_time: [],
		phone: '',
		email: '',
		address: '',
		name: '',
		sections: [],
		section_active: 0,
		section_active_id: 0,
		num_weeks: 1
	},
	mounted() {
		axios.get("/tarifs_sections")
			.then((response) => {
				var data = response.data
				this.sections = data
				data.forEach(function (data) {
					data.tarifs = JSON.parse(data.tarifs)
				});
				if(data[0]['id'])
				this.section_active_id = data[0]['id']
			})
		axios.get("/tarifs_date")
			.then((response) => {
				this.tarifs_date = _.groupBy(response.data, "date")
				this.num_weeks = Object.keys(this.tarifs_date).length / 7
				axios.get("/tarifs_source")
					.then((response) => {
						var data = response.data
						var self = this
						data.forEach(function (data) {
							data.checked = 0
							data.active_day = self.current_day
							data.num_day = 0
							data.summ = 0
							data.current_page = -1
							data.prevVisible = 'not_have'
							if(self.num_weeks <= 1) {
								data.nextVisible = 'not_have'
							}
							else data.nextVisible = ''
							data.disabled = false
							data.days = JSON.parse(data.days)
							data.tarifs.forEach(function (_data) {
								_data.money = JSON.parse(_data.money)
							})
						});
						this.tarifs = data;
					})
			})

		axios.get("/delivery")
			.then((response) => {
				var time = response.data
				var current = this.current_day
				var localLocale = moment()
				localLocale.locale('ru')
				var start_week_full = localLocale.utc().startOf('week')
				var delivery = []
				time.sort(function(a, b) {
					return a['week_number'] - b['week_number'];
				});
				time.forEach((element) => {
					var localLocale = moment("1970-01-01 " + element['time'])
					localLocale.locale('ru')
					var week_time = Number(start_week_full.format('x')) + Number(localLocale.format('x')) + 24 * 60 * 60 * (Number(element['week_number']) - 1) * 1000

					var localLocale2 = moment("1970-01-01 " + element['time2'])
					localLocale2.locale('ru')
					var week_time2 = Number(start_week_full.format('x')) + Number(localLocale2.format('x')) + 24 * 60 * 60 * (Number(element['week_number']) - 1) * 1000

					if (!element['time3']) element['time3'] = ''
					else  element['time3'] = ' ' + element['time3']
					var localLocale3 = moment("1970-01-01" + element['time3'])
					localLocale3.locale('ru')
					var week_time3 = Number(start_week_full.format('x')) + Number(localLocale3.format('x')) + 24 * 60 * 60 * (Number(element['week_number2']) - 1) * 1000

					delivery.push([new Date(week_time), new Date(week_time2), new Date(week_time3)])
				})
				var next_week = new Date(delivery[0][0])
				next_week.setDate(next_week.getDate() + 7)

				var next_week2 = new Date(delivery[0][1])
				next_week2.setDate(next_week2.getDate() + 7)

				var next_week3 = new Date(delivery[0][2])
				next_week3.setDate(next_week3.getDate() + 7)

				delivery.push([next_week, next_week2, next_week3])

				this.delivery_time = delivery
			})
		if (localStorage.getItem('basket')) this.basket = JSON.parse(localStorage.getItem('basket'));
	},
	computed: {
		basketSumm: function () {
			var summ = 0
			this.basket.forEach((item)=>(summ += item[5]))
			return summ
		},
		countBasket: function () {
			return this.basket.length
		},
		nearDate: function () {
			var now = new Date()
			var elem = this.delivery_time.filter(times => times[2] > now)
			return elem[0] ? elem[0] : ''
		},
		getTarifsSection: function () {
			var active_id = this.section_active_id
			var tarifs_source = $.extend(true, [], this.tarifs)
			if (!_.isEmpty(tarifs_source)) {
				tarifs_source.forEach(function(tarifs, index) {
					var _tarifs = tarifs['tarifs']
					var res = []
					_tarifs.forEach(function(tarif) {
						if(tarif.section.includes(active_id.toString()))
							res.push(tarif)
					})
					tarifs_source[index]['tarifs'] = res
				})
			}
			return tarifs_source;
		}
	},
	watch: {
		basket: {
			handler() {
				localStorage.setItem('basket', JSON.stringify(this.basket));
			},
			deep: true,
		},
	},
	methods: {
		checkTarifs(tab_index) {
			return this.getTarifsSection[tab_index]['tarifs'].length
		},
		changeTab(tab_index, index) {
			this.tarifs[tab_index].checked = index
		},
		changeDay(tab_index, index) {
			this.tarifs[tab_index].active_day = index
		},
		changeNumDay(tab_index, day) {
			this.tarifs[tab_index].num_day = day
		},
		changePagePlus(tab_index, e) {
			if ($(e.target).closest('div').hasClass('not_have')) return
			this.tarifs[tab_index].current_page++
			if(this.num_weeks <= this.tarifs[tab_index].current_page + 2) {
				this.tarifs[tab_index].nextVisible = 'not_have'
				this.tarifs[tab_index].prevVisible = '';
				return
			}
			this.updateVisiblePage(tab_index, this.tarifs[tab_index].current_page)
		},
		changePageMinus(tab_index, e) {
			if ($(e.target).closest('div').hasClass('not_have')) return
			this.tarifs[tab_index].current_page--
			this.updateVisiblePage(tab_index, this.tarifs[tab_index].current_page)
		},
		changePage(tab_index, page) {
			if(this.num_weeks <= page + 2) return;
			this.tarifs[tab_index].current_page = page
			this.updateVisiblePage(tab_index, page)
		},
		changeSection(section_index, section_id) {
			this.section_active = section_index
			this.section_active_id = section_id
		},
		updateVisiblePage(tab_index, page) {
			this.tarifs[tab_index].prevVisible = page != -1 ? '' : 'not_have'
			this.tarifs[tab_index].nextVisible = page != 1 ? '' : 'not_have'
		},
		moneyDay(tab_index) {
			var num_day = this.tarifs[tab_index].num_day
			var days_count = this.tarifs[tab_index]['days']
			if (days_count == null) return
			days_count = days_count[num_day]
			return (this.moneySumm(tab_index) / days_count).toFixed(2)
		},
		moneySumm(tab_index) {
			var num_day = this.tarifs[tab_index].num_day
			var money = this.tarifs[tab_index].tarifs[this.tarifs[tab_index].checked]['money']
			if (!money.length) return;
			var res = money.filter((el) => el['day'] == num_day)
			var days_count = this.tarifs[tab_index]['days']
			if (days_count == null) return
			// либо по дате, либо первый в списке дат
			days_count = days_count[num_day]
			var _res = []
			if (res[0]) _res = res[0]
			else _res = money[0]
			var day = _res['day']
			money = _res['money']
			if (!res[0]) money = money * days_count
			return money
		},
		countFood(tab_index) {
			var food = this.tarifs[tab_index].tarifs[this.tarifs[tab_index].checked]
			var count = 0;
			for (var key in food) {
				if (key.includes('food_')) {
					if (food[key] != null) {
						count++;
					}
				}
			}
			return count
		},
		currentFood: function (index) {
			var active_day = this.tarifs[index].active_day
			var tarifs = this.tarifs[index].tarifs
			var callories_tab = this.tarifs[index].checked
			var tarif = tarifs[callories_tab]
			tarif.food = []
			var food = this.tarifs_date[active_day]
			// this.tarifs_date обновляется, поэтому currentFood вызывается еще раз, а это нам не надо, потому что оно равно изначально undefined
			if (!food) return
			var i = 0
			for (var key in tarif) {
				if (key.includes('food_')) {
					if (tarif[key] != null) {
						var val = parseFloat(tarif[key])
						var index = parseInt(key.match(/\d+/g)) - 1
						tarif.food[i] = {
							index: index,
							name: food[index]['name'],
							calories: food[index]['calories'] * val,
							proteins: food[index]['proteins'] * val,
							fats: food[index]['fats'] * val,
							carbohydrates: food[index]['carbohydrates'] * val,
						}
						i++
					}
				}
			}
			return tarif.food
		},
		/**
		 * не показывает другие даты, кот. не входят в текущую страницу
		 * @param current_page
		 * @param index - текущая дата у списка дат тарифа
		 * @returns {boolean}
		 */
		removeNextDays(current_page, index) {
			var date = ''
			// хранит ближайший понедельник этой недели date - 2018-04-30
			for (var prop in this.tarifs_date) {
				date = prop
				break
			}
			return (new Date(index)) <= (new Date(date)).setDate((new Date(date)).getDate() + 7 * (current_page + 2) - 1)
				&& (new Date(index)) > (new Date(date)).setDate((new Date(date)).getDate() + 7 * (current_page + 1) - 1)
		},
		addToBasket: function (tab_index, event) {
			var self = this
			if (self.tarifs[tab_index].disabled) return
			var elem = event.target
			if (self.tarifs[tab_index].active_day < self.current_day) {
				$($(elem).parent()).tooltip({
					title: "Нельзя добавлять блюда, которые раньше текущего дня",
					trigger: 'manual'
				}).tooltip('show')
				setTimeout(function () {
					$(elem).parent().tooltip('hide')
				}, 2000);
				return
			}
			$($(elem).parent()).tooltip({
				title: "Ваш заказ добавлен в <a href='#' data-toggle='modal' data-target='#modalBasket'>корзину</a>",
				html: true,
				trigger: 'manual'
			}).tooltip('show')
			self.tarifs[tab_index].disabled = true

			var callories_page = self.tarifs[tab_index].checked
			var image = self.tarifs[tab_index]['image']
			var name = self.tarifs[tab_index]['name']
			var callories = self.tarifs[tab_index].tarifs[callories_page]['tarif_num']
			var money_day = parseFloat(self.moneyDay(tab_index))
			var summ = parseFloat(self.moneySumm(tab_index))

			var num_day = this.tarifs[tab_index].num_day
			var days_count = this.tarifs[tab_index]['days']
			if (days_count == null) return
			// либо по дате, либо первый в списке дат
			days_count = days_count[num_day]

			self.basket.push([image, name, callories, money_day, days_count, summ])
			setTimeout(function () {
				self.tarifs[tab_index].disabled = false
				$(elem).parent().tooltip('hide')
			}, 4000);

		},
		removeItem: function (index) {
			this.basket.splice(index, 1)
		},
		buyOrder: function (event) {
			if (this.basket.length) {
				var el = event.target
				var form = $(el).closest('form')
				var data = form.serialize();
				var phone = form.find('input[name="phone"]')
				var name = form.find('input[name="name"]')
				var self = this
				if (!phone.val()) phone.addClass('error_input')
				if (!name.val()) name.addClass('error_input')
				if (!phone.val() || !name.val()) return

				axios.post(form.attr('action'), data)
					.then(function (response) {
						if (response) {
							$('#modalBasket').modal('hide')
							$('#modalThanks').modal()
							self.basket = []
							$('#modalBasket form')[0].reset()
							if(google_id) gtag('event', 'sendOrder', { event_category: 'order', event_action: 'click' });
							if(yandex_id) eval(`yaCounter${yandex_id}`).reachGoal('madeOrder')
							if(fb_pixel_id) fbq('track', 'Purchase', {
								value: 0,
								currency: 'RUB',
							});
							if(vk_pixel_madeOrder) {
								VK.Retargeting.Event('madeOrder')
								VK.Retargeting.Add(vk_pixel_madeOrder)
							}
						}
					})
					.catch(function (error) {
						console.log(error);
					});
			}
		},
		callClick: function (event) {
			var el = event.target
			var form = $(el).closest('form')
			var data = form.serialize()
			var phone = form.find('input[name="phone"]')
			var name = form.find('input[name="name"]')
			phone.removeClass('error_input')
			name.removeClass('error_input')
			if (!phone.val()) phone.addClass('error_input')
			if (!name.val()) name.addClass('error_input')

			if (!phone.val() || !name.val()) return

			axios.post(form.attr('action'), data)
				.then(function (response) {
					if (response) {
						$('#modalCallback').modal('hide')
						$('#modalThanks').modal()
						phone.val('')
						name.val('')
						if(google_id) gtag('event', 'sendCall', { event_category: 'order_call', event_action: 'click' });
						if(yandex_id) eval(`yaCounter${yandex_id}`).reachGoal('call')
						if(fb_pixel_id) fbq('track', 'Lead')
						if(vk_pixel_call) {
							VK.Retargeting.Event('call')
							VK.Retargeting.Add(vk_pixel_call)
						}
					}
				})
				.catch(function (error) {
					console.log(error)
				});
		},
		showDay: function (value, index) {
			if (value) {
				return value[index] ? value[index] : ''
			} else return '';
		},
	},
	filters: {
		/**
		 * filter shown only day and month
		 */
		onlyDM: function (value) {
			if (value) {
				var localLocale = moment(value, 'YYYY-MM-DD')
				localLocale.locale('ru')
				return _.capitalize(localLocale.format('ddd DD.MM'))
			}
		},
		onlyDay: function (value) {
			if (value) {
				var localLocale = moment(value, 'YYYY-MM-DD')
				localLocale.locale('ru')
				return localLocale.format('DD')
			}
		},
		onlyMonth: function (value) {
			if (value) {
				var localLocale = moment(value, 'YYYY-MM-DD')
				localLocale.locale('ru')
				return localLocale.format('MM')
			}
		},
		onlyYear: function (value) {
			if (value) {
				var localLocale = moment(value, 'YYYY-MM-DD')
				localLocale.locale('ru')
				return localLocale.format('YYYY')
			}
		},
		russianDate: function (value) {
			if (value) {
				var localLocale = moment(value, 'YYYY-MM-DD')
				localLocale.locale('ru')
				return localLocale.format('DD.MM.YYYY')
			}
		},
		onlyTime: function (value) {
			if (value) {
				var localLocale = moment(value, 'YYYY-MM-DD')
				localLocale.locale('ru')
				return localLocale.format('HH:mm')
			}
		}
	}
})
