$(window).ready(function () {

	changeHeightBody();
	function changeHeightBody() {
		var height = $('.header').outerHeight();
		$('body').css('padding-top', height);
	}

	function scroll() {
		if($(window).scrollTop() > 0) $('.header').addClass('scroll')
		else $('.header').removeClass('scroll')
	}

	$(window).scroll(function() {
		scroll();
	});

	$(window).resize(function() {
		changeHeightBody();
	});

	var slider = $('.slider .owl-carousel')

	swipe(slider.owlCarousel({
		loop: true,
		margin: 10,
		nav: false,
		autoplay: true,
		autoplayTimeout: 5000,
		autoplayHoverPause: true,
		autoplaySpeed: 500,
		items: 1,
		touchDrag: false,
		mouseDrag: false
	}))

	function swipe(el) {
		el.swipe({
			swipe: function (event, direction, distance, duration, fingerCount, fingerData) {
				if (direction == "left") el.trigger('next.owl.carousel')
				if (direction == "right") el.trigger('prev.owl.carousel')
				return true
			}, allowPageScroll: "auto"
		});
	}

	swipe($('.instruction .owl-carousel').owlCarousel({
		loop: false,
		margin: 10,
		nav: false,
		autoplay: false,
		autoplayTimeout: 5000,
		autoplayHoverPause: true,
		autoplaySpeed: 1000,
		items: 3,
		responsive: {
			0: {
				items: 1,
			},
			600: {
				items: 2,
			},
			1000: {
				items: 3,
			},
		},
		touchDrag: false,
		mouseDrag: false
	}))

	swipe($('.partners .owl-carousel').owlCarousel({
		loop: false,
		margin: 10,
		nav: false,
		items: 6,
		autoplay: true,
		autoplaySpeed: 1000,
		autoplayTimeout: 4000,
		responsive: {
			0: {
				items: 1,
			},
			600: {
				items: 3,
			},
			1000: {
				items: 5,
			},
			1300: {
				items: 6,
			}
		},
		touchDrag: false,
		mouseDrag: false,
		onRefreshed: function (event) {
			if(event.page.size < event.item.count) {
				 $(event.target).find('.owl-stage').removeClass('owl-stage_full')
			} else {
				$(event.target).find('.owl-stage').addClass('owl-stage_full')
			}
		},
	}))


	swipe($('.feedback .owl-carousel').owlCarousel({
		loop: true,
		margin: 10,
		nav: false,
		autoplay: false,
		autoplayTimeout: 4000,
		autoplayHoverPause: true,
		autoplaySpeed: 1000,
		items: 1,
		touchDrag: false,
		mouseDrag: false
	}))

	$(".img_compare").twentytwenty({before_label: 'До', after_label: 'После'})

	swipe($('.kitchen .owl-carousel').owlCarousel({
		loop: true,
		margin: 10,
		nav: false,
		autoplay: false,
		autoplayTimeout: 5000,
		autoplayHoverPause: true,
		autoplaySpeed: 1000,
		items: 1,
		touchDrag: false,
		mouseDrag: false
	}))

	var styleElem = document.head.appendChild(document.createElement('style'));
	var count = 0;
	$('.slider ul li[id*=slide-]').each(function (index) {
		$(styleElem).append('.slider_' + index + ':after {background-image:url(' + $(this).attr('data-img') + ')}')
		count++;
	});

	$('.slider').addClass('slider_0')

	slider.on('translated.owl.carousel', function (property) {

		for (var i = 0; i < count; i++) {
			$('.slider').removeClass('slider_' + i)
		}

		$('.slider').addClass('slider_' + property.page.index)

		$('.slider ul li.main-fcolor').removeClass('main-fcolor');
		$('.slider ul li#slide-' + property.page.index).addClass('main-fcolor');

		$id_theme = $('.slider ul li#slide-' + property.page.index).attr('data-theme');
		if ($id_theme == 1) {
			$('.slider').removeClass('black').addClass('white');
		} else {
			$('.slider').removeClass('white').addClass('black');
		}
	});

	$('input[name="phone"]').inputmask(mask_phone)

	$('#modalCallback').on('hidden.bs.modal', function () {
		$(this).find('input[name="phone"]').removeClass('error_input').val('')
		$(this).find('input[name="name"]').removeClass('error_input').val('')
	})

	$('a[data-target="#modalContact"]').click(function () {
		if ($(window).width() <= 1000) {
			if ($('.call_modal').css('display') == 'block') $('.call_modal').css('display', 'none');
			else $('.call_modal').css('display', 'block');
			return false;
		}
	});

	$('#modalBasket').on('hidden.bs.modal', function () {
		$(this).find('input[name="phone"]').removeClass('error_input').val('')
		$(this).find('input[name="name"]').removeClass('error_input').val('')
	})

	$('body').removeClass('hide_page')
	$('.spinner').remove()

});
