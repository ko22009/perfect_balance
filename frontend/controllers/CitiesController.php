<?php

namespace frontend\controllers;

use app\models\Cities;
use yii\base\Controller;
use yii\web\Response;

class CitiesController extends Controller {
    /**
     * Возвращает дату начиная с текущей недели понедельника по восресенье третьей недели
     */
    public function actionIndex() {
        \Yii::$app->response->format = Response:: FORMAT_JSON;
        $request = \Yii::$app->request;
        $latitude = $request->post('latitude');
        $longitude = $request->post('longitude');
        $cities = Cities::find()->select(['latitude', 'longitude', 'domen'])->all();
        $near_id = $this->NearestCity($latitude, $longitude, $cities);
        return '//' . $cities[$near_id]['domen'] . '.' . \Yii::$app->request->hostName;
    }

    /**
     * Convert Degress to Radians
     */
    function Deg2Rad($deg) {
        return $deg * M_PI / 180;
    }

    function PythagorasEquirectangular($lat1, $lon1, $lat2, $lon2) {
        $lat1 = $this->Deg2Rad($lat1);
        $lat2 = $this->Deg2Rad($lat2);
        $lon1 = $this->Deg2Rad($lon1);
        $lon2 = $this->Deg2Rad($lon2);
        $R = 6371; //km
        $x = ($lon2 - $lon1) * cos(($lat1 + $lat2) / 2);
        $y = ($lat2 - $lat1);
        $d = sqrt($x * $x + $y * $y) * $R;
        return $d;
    }

    function NearestCity($latitude, $longitude, $cities) {
        $mindif = 99999;
        $closest = 0;
        for ($index = 0; $index < count($cities); $index++) {
            $dif = $this->PythagorasEquirectangular($latitude, $longitude, $cities[$index]['latitude'], $cities[$index]['longitude']);
            if ($dif < $mindif) {
                $closest = $index;
                $mindif = $dif;
            }
        }
        return $closest;
    }

}