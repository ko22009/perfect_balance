<?php

namespace frontend\controllers;

use app\models\Cities;
use yii\base\Controller;
use yii\web\Response;

class AmoController extends Controller {
    /**
     * Перезвонить
     */
    public function actionCall() {
        $match = preg_match('/(\w+)\.(\w+\.\w+)/', \Yii::$app->request->hostName, $matches);
        if ($match) {
            $city = Cities::findOne(['domen' => $matches[1]]);
        }
        else $city = Cities::findOne(['name' => 'Магнитогорск']);
        \Yii::$app->response->format = Response:: FORMAT_JSON;
        $request = \Yii::$app->request;

        $phone = $request->post('phone');
        $name = $request->post('name');

        try {
            $amo = \Yii::$app->amocrm->getClient();

            $lead = $amo->lead;
            $lead['name'] = 'Перезвонить';
            $lead['status_id'] = 18486823;
            $lead['responsible_user_id'] = $city['user_id_amo_crm'];
            $new_lead = $lead->apiAdd();

            $contact = $amo->contact;
            $contact['name'] = $name;
            $contact['tags'] = ['перезвонить', 'сайт'];
            $contact['responsible_user_id'] = $city['user_id_amo_crm'];
            $contact->addCustomField(400769, [
                [$phone, 'WORK'],
            ]);
            $contact['linked_leads_id'] = $new_lead;
            $contact->apiAdd();

            $task = $amo->task;
            $task['element_id'] = $new_lead;
            $task['element_type'] = 2;
            $task['task_type'] = 1;
            $task['text'] = "Перезвонить";
            $task['responsible_user_id'] = $city['user_id_amo_crm'];
            $task->apiAdd();

            return true;
        } catch (\AmoCRM\Exception $e) {
            printf('Error (%d): %s' . PHP_EOL, $e->getCode(), $e->getMessage());
        }
        return false;
    }

    /**
     * Перезвонить
     */
    public function actionOrder() {
        $match = preg_match('/(\w+)\.(\w+\.\w+)/', \Yii::$app->request->hostName, $matches);
        if ($match) {
            $city = Cities::findOne(['domen' => $matches[1]]);
        }
        else $city = Cities::findOne(['name' => 'Магнитогорск']);
        \Yii::$app->response->format = Response:: FORMAT_JSON;
        $request = \Yii::$app->request;
        $phone = $request->post('phone');
        $name = $request->post('name');
        $address = $request->post('address');
        $email = $request->post('email');
        $date = $request->post('delivery_date');
        $tarif = $request->post('tarif');
        try {

            for ($i = 0; $i < count($tarif); $i++) {

                $amo = \Yii::$app->amocrm->getClient();
                $lead = $amo->lead;
                $lead['name'] = 'Оформить заказ';
                $lead['status_id'] = 18486823;
                $lead['price'] = $tarif[$i][3];
                $lead['responsible_user_id'] = $city['user_id_amo_crm'];
                // address
                $lead->addCustomField(559411, $address, false, 'address_line_1');
                $lead->addCustomField(559463, $tarif[$i][0] . ' - ' . $tarif[$i][4]);
                // time of delivery
                $lead->addCustomField(559443, $date);
                // count days
                $lead->addCustomField(559469, $tarif[$i][1]);
                // day costs
                $lead->addCustomField(559471, $tarif[$i][2]);

                $new_lead = $lead->apiAdd();

                $contact = $amo->contact;
                $contact['name'] = $name;
                $contact['tags'] = ['оформить заказ', 'сайт'];
                $contact['responsible_user_id'] = $city['user_id_amo_crm'];

                // email
                $contact->addCustomField(400771, [
                    [$email, 'WORK'],
                ]);
                // phone
                $contact->addCustomField(400769, [
                    [$phone, 'WORK'],
                ]);
                $contact['linked_leads_id'] = $new_lead;
                $contact->apiAdd();

                $task = $amo->task;
                $task['element_id'] = $new_lead;
                $task['element_type'] = 2;
                $task['task_type'] = 1;
                $task['text'] = "Оформить сделку";
                $task['responsible_user_id'] = $city['user_id_amo_crm'];
                $task->apiAdd();
            }

            return true;
        } catch (\AmoCRM\Exception $e) {
            printf('Error (%d): %s' . PHP_EOL, $e->getCode(), $e->getMessage());
        }
        return false;
    }
}
