<?php

namespace frontend\controllers;

use yii\base\Controller;
use yii\web\Response;
use app\models\TarifsSections;

class Tarifs_sectionsController extends Controller
{
    public function actionIndex()
    {
        \Yii::$app->response->format = Response:: FORMAT_JSON;
        $session = \Yii::$app->session;
        $city_id = $session['city_id'];
        $tarifs_sections = TarifsSections::find()->where(['city_id' => $city_id])->all();
        return $tarifs_sections;
    }
}
