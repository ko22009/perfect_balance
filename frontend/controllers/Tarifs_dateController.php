<?php

namespace frontend\controllers;

use app\models\Tarifs_date;
use yii\base\Controller;
use yii\web\Response;

class Tarifs_dateController extends Controller {
    /**
     * Возвращает дату начиная с текущей недели понедельника по восресенье третьей недели
     */
    public function actionIndex() {
        date_default_timezone_set('Asia/Yekaterinburg');
        \Yii::$app->response->format = Response:: FORMAT_JSON;
        $weekday = date("w") - 1;
        if ($weekday < 0) {
            $weekday += 7;
        }
        $session = \Yii::$app->session;
        $city_id = $session['city_id'];
        $monday_this_week = date("Y-m-d", strtotime(date("Y-m-d")) - $weekday * 86400);
        $monday_three_week = date("Y-m-d", strtotime(date("Y-m-d")) - $weekday * 86400 + (3 * 7 - 1) * 86400);
        $tarifs_date = Tarifs_date::find()
            ->where(['>=', 'date', $monday_this_week])
            ->andWhere(['<=', 'date', $monday_three_week])
            ->andWhere(['city_id' => $city_id])
            ->orderBy([
                'date' => SORT_ASC,
                'id' => SORT_ASC
            ])
            ->all();
        return $tarifs_date;
    }
}
