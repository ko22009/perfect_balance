<?php
namespace frontend\controllers;

use app\models\Cities;
use app\models\Contacts;
use app\models\Crew;
use app\models\Kitchens;
use app\models\Partners;
use app\models\Reviews;
use app\models\Slides;
use frontend\models\SignupForm;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    /*public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }*/

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $base_domain = \Yii::$app->params['base_domain'];
        $match = preg_match('/(\w+)\.' . preg_quote($base_domain) . '/', \Yii::$app->request->hostName, $matches);
        if ($match) {
            $city_id = Cities::findOne(['domen' => $matches[1]])['id'];
        }
        else $city_id = Cities::findOne(['name' => 'Магнитогорск'])['id'];

        if(isset($matches[2])) $base_domen = $matches[2];
        else $base_domen = \Yii::$app->request->hostName;

        $city = Cities::findOne(['id' => $city_id]);
        $session = \Yii::$app->session;
        $session->set('city_id', $city_id);
        $city_list = Cities::find()->all();
        $slider = Slides::find()->where(['city_id' => $city_id])->all();
        $crew = Crew::find()->where(['city_id' => $city_id])->all();
        $kitchens = Kitchens::find()->where(['city_id' => $city_id])->all();
        $contacts = Contacts::findOne(['city_id' => $city_id]);
        $partners = Partners::find()->where(['city_id' => $city_id])->all();
        $reviews = Reviews::find()->where(['city_id' => $city_id])->all();

        if($city['down']) {
            return $this->render('down', ['city_list' => $city_list, 'city' => $city, 'base_domen' => $base_domen]);
        }

        Yii::$app->view->params['google_id'] = $city['google_id'];
        Yii::$app->view->params['yandex_id'] = $city['yandex_id'];
        Yii::$app->view->params['fb_pixel_id'] = $city['fb_pixel_id'];
        Yii::$app->view->params['vk_pixel_id'] = $city['vk_pixel_id'];
        Yii::$app->view->params['vk_pixel_view'] = $city['vk_pixel_view'];
        Yii::$app->view->params['vk_pixel_madeOrder'] = $city['vk_pixel_madeOrder'];
        Yii::$app->view->params['vk_pixel_call'] = $city['vk_pixel_call'];
        if(isset(\Yii::$app->params['mask_phone'])) Yii::$app->view->params['mask_phone'] = \Yii::$app->params['mask_phone'];

        return $this->render('index', [
            'city' => $city,
            'slider' => $slider,
            'crew' => $crew,
            'kitchens' => $kitchens,
            'contacts' => $contacts,
            'partners' => $partners,
            'reviews' => $reviews,
            'city_list' => $city_list
        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    /*public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }*/

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    /*public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }*/

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        $base_domain = \Yii::$app->params['base_domain'];
        $match = preg_match('/(\w+)\.' . preg_quote($base_domain) . '/', \Yii::$app->request->hostName, $matches);
        if ($match) {
            $city_id = Cities::findOne(['domen' => $matches[1]])['id'];
        } else $city_id = null;
        $params = Yii::$app->request->post();
        $params = array_replace_recursive($params, ["SignupForm" => ["city_id" => $city_id]]);
        if ($model->load($params)) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    /*public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }*/

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    /*public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }*/
}
