<?php

namespace frontend\controllers;

use app\models\Delivery;
use yii\base\Controller;
use yii\web\Response;

class DeliveryController extends Controller {
    /**
     * Возвращает дату начиная с текущей недели понедельника по восресенье третьей недели
     */
    public function actionIndex() {
        \Yii::$app->response->format = Response:: FORMAT_JSON;
        $session = \Yii::$app->session;
        $city_id = $session['city_id'];
        $delivery = Delivery::findAll(['city_id' => $city_id]);
        return $delivery;
    }
}
