<?php

namespace frontend\controllers;

use app\models\Tarifs_source;
use yii\base\Controller;
use yii\web\Response;

class Tarifs_sourceController extends Controller {
    public function actionIndex() {

        \Yii::$app->response->format = Response:: FORMAT_JSON;
        $session = \Yii::$app->session;
        $city_id = $session['city_id'];
        /**
         * Возвращает к определенным тарифам источникам определенные секции
         * по городу
         */
        $tarifs_source_1 = Tarifs_source::find()->joinWith('tarifs_sections')->where(['tarifs_source.city_id' => $city_id])->select(['tarifs_sections.*', 'tarifs_source.*'])->asArray()->all();

        /**
         * Возвращает список тарифов источников, внутрь которых вложены тарифы
         */
        $tarifs_source_2 = Tarifs_source::find()->where(['tarifs_source.city_id' => $city_id])->joinWith(['tarifs' => function($query) use ($city_id) {
            $query->andFilterWhere(['tarifs.city_id' => $city_id]);
        }])->joinWith(['tarifs_sections' => function($query) use ($city_id) {
            $query->andFilterWhere(['tarifs_sections.city_id' => $city_id]);
        }])->orderBy(['tarifs.tarif_num' => SORT_ASC])->asArray()->all();

        for ($i = 0; $i < count($tarifs_source_2); $i++) {
            $tarifs = $tarifs_source_1[$i]['tarifs_sections'];
            for ($z = 0; $z < count($tarifs_source_2[$i]['tarifs']); $z++) {
                $tarifs_source_2[$i]['tarifs'][$z]['section'] = [];
                if (count($tarifs)) {
                    for ($j = 0; $j < count($tarifs); $j++) {
                        $_tarifs = json_decode($tarifs[$j]['tarifs']);
                        if (in_array($tarifs_source_2[$i]['tarifs'][$z]['id'], $_tarifs)) {
                           array_push($tarifs_source_2[$i]['tarifs'][$z]['section'], $tarifs[$j]['id']);
                        }
                    }
                }
            }
        }
        return $tarifs_source_2;
    }
}
