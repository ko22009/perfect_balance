<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tarifs_source".
 *
 * @property int $id
 * @property string $name
 * @property int $order
 */
class Tarifs_source extends \common\models\Tarifs_source
{
    public function fields()
    {
        return array_merge(parent::fields(), [
            'tarifs',
        ]);
    }
}
