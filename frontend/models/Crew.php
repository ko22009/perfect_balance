<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "crew".
 *
 * @property int $id
 * @property int $city_id
 * @property string $name
 * @property string $position
 * @property string $quote
 * @property string $image
 */
class Crew extends \common\models\Crew
{
}
