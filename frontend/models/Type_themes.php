<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "type_themes".
 *
 * @property int $id
 * @property string $name
 * @property int $indicator
 */
class Type_themes extends \common\models\Type_themes
{
}