<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reviews".
 *
 * @property int $id
 * @property int $city_id
 * @property string $name
 * @property string $description
 * @property string $terif
 * @property string $result
 * @property string $text
 * @property string $before_image
 * @property string $after_image
 */
class Reviews extends \common\models\Reviews
{
}
