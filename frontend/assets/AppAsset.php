<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        "css/bootstrap.min.css",
        "css/owl.carousel.min.css",
        "css/owl.theme.default.css",
        "css/animate.css",
        "css/jquery-ui.min.css",
        "css/fontawesome-all.min.css",
        "css/twentytwenty.css",
        "css/main.css",
    ];
    public $js = [
        "js/jquery-ui.min.js",
        "js/bootstrap.bundle.min.js",
        "js/owl.carousel.min.js",
        "js/owl.animate.js",
        "js/jquery.event.move.js",
        "js/jquery.twentytwenty.js",
        "js/axios.min.js",
        "js/lodash.min.js",
        "js/moment-with-locales.js",
        "js/jquery.inputmask.js",
        "js/jquery.touchSwipe.min.js",
        "js/main.js",
        "js/vue_dev.js",
        'js/vue_code.js'
    ];
    public $depends = [
        'yii\web\YiiAsset'
    ];
}
