<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,300i,400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700&amp;subset=cyrillic"
          rel="stylesheet">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?php

    if(isset($this->params['google_id'])) $google_id = $this->params['google_id'];
    else $google_id = '';
    if(isset($this->params['yandex_id'])) $yandex_id = $this->params['yandex_id'];
    else $yandex_id = '';
    if(isset($this->params['fb_pixel_id'])) $fb_pixel_id = $this->params['fb_pixel_id'];
    else $fb_pixel_id = '';

    if(isset($this->params['vk_pixel_id'])) $vk_pixel_id = $this->params['vk_pixel_id'];
    else $vk_pixel_id = '';
    if(isset($this->params['vk_pixel_view'])) $vk_pixel_view = $this->params['vk_pixel_view'];
    else $vk_pixel_view = '';
    if(isset($this->params['vk_pixel_madeOrder'])) $vk_pixel_madeOrder = $this->params['vk_pixel_madeOrder'];
    else $vk_pixel_madeOrder = '';
    if(isset($this->params['vk_pixel_call'])) $vk_pixel_call = $this->params['vk_pixel_call'];
    else $vk_pixel_call = '';

    if($google_id) {
        echo $this->render('//site/partials/google_analytics', ['google_id' => $google_id]);
    }
    if($yandex_id) {
        echo $this->render('//site/partials/yandex_metrica', ['yandex_id' => $yandex_id]);
    }
    if($fb_pixel_id) {
        echo $this->render('//site/partials/_fb_pixel', ['fb_pixel_id' => $fb_pixel_id]);
    }
    if($vk_pixel_id && $vk_pixel_view) {
        echo $this->render('//site/partials/_vk_pixel', ['vk_pixel_id' => $vk_pixel_id, 'vk_pixel_view' => $vk_pixel_view]);
    }
    if(isset(\Yii::$app->params['mask_phone'])) $mask_phone = \Yii::$app->params['mask_phone'];
    else $mask_phone = '+7 (999) 999-99-99';
    ?>

    <script>
        var yandex_id = '<?=$yandex_id?>'
        var google_id = '<?=$google_id?>'
        var fb_pixel_id = '<?=$fb_pixel_id?>'
        var vk_pixel_madeOrder = '<?=$vk_pixel_madeOrder?>'
        var vk_pixel_call = '<?=$vk_pixel_call?>'
        var mask_phone = '<?=$mask_phone?>'
    </script>
</head>
<body class="hide_page">
<div class="spinner">
    <div class="lds-spinner">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>
<?php $this->beginBody() ?>

<?= $content ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
