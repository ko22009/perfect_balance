<?php
if (count($crew) > 0) {
    ?>
    <div class="workers">
        <h2 class="text-center heading">Наша команда</h2>
        <div class="item">
            <div class="container-fluid">
                <div class="row" style="align-items: center;justify-content: center; text-align: center;">
                    <?php
                    $wrap = '';
                    for ($i = 0; $i < count($crew); $i++) {
                        $image = Yii::$app->urlManagerBackend->baseUrl . '/uploads/crews/' . $crew[$i]['image'];
                        $name = $crew[$i]['name'];
                        $position = $crew[$i]['position'];
                        $quote = $crew[$i]['quote'];
                        $quote = nl2br($quote);
                        $wrap .= '
                        <div>
                            <div class="img-i">
                                <img src="' . $image . '">
                            </div>
                            <h3 class="r-l">' . $name . '</h3>
                            <h4 class="c-gold r-r">' . $position . '</h4>
                            <p class="r-l">' . $quote . '</p>
                        </div>
                        
                    ';
                    }
                    echo $wrap;
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>