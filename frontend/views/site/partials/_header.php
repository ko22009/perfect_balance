<header class="header">
    <div class="container-fluid">
        <nav>
        <span>
        <div class="current-city">
        <span class="fs-12 fw-t">Ваш город:</span>
        <a data-toggle="modal" data-target="#modalcity" class="city-select fs-14 fw-m main-fcolor" href="#"><?= $city_name ?></a>
        </div>
        <div class="social">
            <?php
            $vk = $contacts['vk'];
            $instagram = $contacts['instagram'];
            $fb = $contacts['fb']; ?>
        <?php if($vk) {?>
            <span><a target="_blank" href="<?=$vk?>"><img src="img/social/vk-logo.png"></a></span>
        <?php } ?>
        <?php if($instagram) {?>
            <span><a target="_blank" href="<?=$instagram?>"><img src="img/social/instagram-logo.png"></a></span>
        <?php } ?>
            <?php if($fb) {?>
                <span><a target="_blank" href="<?=$fb?>"><img src="img/social/fb-logo.png"></a></span>
            <?php } ?>
        </div>
        <div class="call-order btn-click py-1 px-3 main-bgcolor bor-r10">
        <a data-toggle="modal" data-target="#modalCallback" class="fs-14 fw-t " href="#">Заказать звонок</a>
        </div>
        </span>
            <div class="logo"><img src="img/logo-main.png"></div>
            <span>
        <div>
        <div class="fs-12 fw-t"><?= $city_name ?></div>
        <div><a class="phone" href="tel:<?= $phone ?>"><?= $phone ?></a></div>
        <div class="fs-12 fw-t"><?= $time_text ?></div>
        </div>

        <?php if($common_phone) { ?>
            <div>
               <div class="fs-12 fw-t"><?=$common_phone['country']?></div>
               <div><a class="phone" href="tel:<?=$common_phone['number']?>"><?=$common_phone['number']?></a></div>
               <div class="fs-12 fw-t"><?=$common_phone['time']?></div>
           </div>
        <?php } ?>

        <div id="basket">
        <a data-toggle="modal" data-target="#modalBasket"
           style="display: inline-block;text-decoration: none;color:black" href="#">
        <span class="icon main-bgcolor">
        <img src="img/icon_basket.png">
        </span>
        <span class="text">
        <span class="main-fcolor">Сумма:</span> <span>{{basketSumm}} <?=$money_symbol_text?></span>
        </span>
        </a>
        <div class="h-bgc"></div>
        <span class="count">{{countBasket}}</span>
        </div>
        </span>
        </nav>
        <nav class="mobile">
            <div class="header-top">
                <span>
                    <div class="current-city">
                    <span class="fs-12 fw-t">Ваш город:</span>
                    <a data-toggle="modal" data-target="#modalcity" class="city-select fs-14 fw-m main-fcolor" href="#"><?= $city_name ?></a>
                    </div>
                    <div class="social social_desktop">
                          <?php if($vk) {?>
                              <span><a target="_blank" href="<?=$vk?>"><img src="img/social/vk-logo.png"></a></span>
                          <?php } ?>
                        <?php if($instagram) {?>
                            <span><a target="_blank" href="<?=$instagram?>"><img src="img/social/instagram-logo.png"></a></span>
                        <?php } ?>
                        <?php if($fb) {?>
                            <span><a target="_blank" href="<?=$fb?>"><img src="img/social/fb-logo.png"></a></span>
                        <?php } ?>
                    </div>
               </span>
            </div>
            <div>
                 <span>
                     <div class="modal-contact py-1 px-3 btn-click  bor-r10">
                         <a data-toggle="modal" data-target="#modalContact" class="fs-14 fw-t " href="#">
                             <img src="img/ph.png">
                         </a>
                    </div>
                <div class="call-order py-1 px-3 main-bgcolor bor-r10">
                    <a data-toggle="modal" data-target="#modalCallback" class="fs-14 fw-t " href="#">Заказать звонок</a>
                </div>
                 <div class="phone">
                    <div class="fs-12 fw-t"><?= $city_name ?></div>
                    <div><a class="phone" href="tel:<?= $phone ?>"><?= $phone ?></a></div>
                    <div class="fs-12 fw-t"><?= $time_text ?></div>
                </div>
               </span>
                <div class="logo"><img src="img/logo-main.png"></div>
                <span>
                    <?php if($common_phone) { ?>
                   <div class="phone">
                       <div class="fs-12 fw-t"><?=$common_phone['country']?></div>
                       <div><a class="phone" href="tel:<?=$common_phone['number']?>"><?=$common_phone['number']?></a></div>
                       <div class="fs-12 fw-t"><?=$common_phone['time']?></div>
                   </div>
                    <?php } ?>
                   <div id="basket">
                       <a data-toggle="modal" data-target="#modalBasket"
                          style="display: inline-block;text-decoration: none;color:black" href="#">
                           <span class="icon main-bgcolor">
                           <img src="img/icon_basket.png">
                       </span>
                       <span class="text">
                           <span class="main-fcolor">Сумма:</span> <span>{{basketSumm}} <?=$money_symbol_text?></span>
                       </span>
                       </a>
                        <div class="h-bgc"></div>
                       <span class="count">{{countBasket}}</span>
                   </div>
               </span>
            </div>
        </nav>
    </div>
    <div class="call_modal">
        <div class="container-fluid">
        <div class="social" style="display: flex;justify-content: flex-end;margin-top: 10px;">
            <?php if($vk) {?>
                <span><a target="_blank" href="<?=$vk?>"><img src="img/social/vk-logo.png"></a></span>
            <?php } ?>
            <?php if($instagram) {?>
                <span><a target="_blank" href="<?=$instagram?>"><img src="img/social/instagram-logo.png"></a></span>
            <?php } ?>
            <?php if($fb) {?>
                <span><a target="_blank" href="<?=$fb?>"><img src="img/social/fb-logo.png"></a></span>
            <?php } ?>
        </div>
        <div class="call_modal_row">
        <div class="phone">
            <div class="fs-12 fw-t"><?= $city_name ?></div>
            <div><a class="phone" href="tel:<?= $phone ?>"><?= $phone ?></a></div>
            <div class="fs-12 fw-t"><?= $time_text ?></div>
        </div>
        <div></div>
        <?php if($common_phone) { ?>
            <div class="phone">
                <div class="fs-12 fw-t"><?=$common_phone['country']?></div>
                <div><a class="phone" href="tel:<?=$common_phone['number']?>"><?=$common_phone['number']?></a></div>
                <div class="fs-12 fw-t"><?=$common_phone['time']?></div>
            </div>
        <?php } ?>
        </div>
        </div>
    </div>
</header>
