<?php
if(isset($slider[0]['theme']['indicator']) && $slider[0]['theme']['indicator'] == 1)
    $name = 'white';
else $name = 'black';

if(count($slider) > 0) {
?>
<div class="slider <?=$name?>">
    <!--<input id="button2" type="button" value="Progress" />-->
    <div class="container-fluid">
        <?php
        $wrap = '';
        for($i = 0; $i < count($slider); $i++)
        {
            if($i == 0) {
                echo '<ul>';
                $color = 'class="main-fcolor"';
            } else $color = '';
            $num = $i + 1;
            $image = Yii::$app->urlManagerBackend->baseUrl . '/uploads/slides/' . $slider[$i]['image'];
            $indicator = $slider[$i]['theme']['indicator'];
            echo '<li data-theme="' . $indicator . '" data-img="' . $image . '" id="slide-' . $i . '" ' . $color . ' onclick="$(\'.slider .owl-carousel\').trigger( \'to.owl.carousel\', [' . $i  . ',1,true] );">' . $num . ' - ' . $slider[$i]['nav_title'] . '</li>';
            if($i == count($slider) - 1) {
                echo '</ul>';
            }
            if($slider[$i]['discount']) {
                $discount = '<span class="main-fcolor"> ' . $slider[$i]['discount'] . '</span>';
            } else $discount = '';
            if($slider[$i]['link']) {
                $link = '<a href="#tarif_' . $slider[$i]['link'] . '" class="go-to-menu bor-r10 btn-click"> Перейти к тарифам</a>';
            } else $link = '';
            if($indicator == 1) {
                $white = 'c-white';
            } else $white = '';
            $wrap .= '<div class="item" id="slide-item-0">
                <div class="wrap">' .
                '<h1 class="' . $white . '">' . $slider[$i]['title'] . '</h1>' .
                '<h2 class="' . $white . '">' . $slider[$i]['subtitle'] . $discount . '</h2>' .
                $link .
                '</div></div>';
        }
        ?>

        <div class="owl-carousel owl-theme">
            <?=$wrap?>
        </div>

        <!--<div class="btn-dots">-->
        <!--<span>-->
        <!--<div></div>-->
        <!--<div></div>-->
        <!--<div></div>-->
        <!--</span>-->
        <!--</div>-->
    </div>
</div>
<?php } ?>