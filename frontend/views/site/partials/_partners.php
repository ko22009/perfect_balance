<?php
if(count($partners) > 0) {
?>
<div class="partners pt-100 pb-100">
    <div class="container-fluid">
        <h2 class="text-center heading">Наши партнеры</h2>

        <div class="owl-carousel owl-theme">
            <?php
            for($i = 0; $i < count($partners); $i++)
            {
                echo '<div class="item">';
                $image = Yii::$app->urlManagerBackend->baseUrl . '/uploads/partners/' . $partners[$i]['image'];
                echo '<a href="' . $partners[$i]['link'] . '">';
                echo '<img src="' . $image .'">';
                echo '</a>';
                echo '</div>';
            }
            ?>
        </div>
    </div>
</div>
<?php } ?>