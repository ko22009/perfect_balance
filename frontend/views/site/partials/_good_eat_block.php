<div class="instruction pt-100 pb-100">
    <div class="container-fluid">
        <h2 class="text-center heading">Питаться правильно-просто и легко!</h2>

        <div class="steps-wrap">
            <div class="step">
                <div class="num"><span>01</span><img src="img/step1-img.png"></div>
                <div class="info">Выбираете тариф под Ваши цели</div>

            </div>

            <div class="step">
                <div class="num"><span>02</span><img src="img/step2-img.png"></div>
                <div class="info"><span>Мы уточняем детали заказа
                       по телефону и передаем его
                       на производство</span></div>

            </div>

            <div class="step">
                <div class="num"><span>03</span><img src="img/step3-img.png"></div>
                <div class="info"><span>Мы готовим ваш комплект еды
                       и раскладываем по порциям</span></div>

            </div>

            <div class="step">
                <div class="num"><span>04</span><img src="img/step4-img.png"></div>
                <div class="info">
                       <span>Курьер доставляет его
                       к вам домой или в офис</span></div>

            </div>
        </div>
        <div class="owl-carousel owl-theme">

            <div class="item">
                <div class="step">
                    <div class="num"><span>01</span><img src="img/step1-img.png"></div>
                    <div class="info">Выбираете тариф под Ваши цели</div>

                </div>
            </div>
            <div class="item">
                <div class="step">
                    <div class="num"><span>02</span><img src="img/step2-img.png"></div>
                    <div class="info"><span>Мы уточняем детали заказа
                       по телефону и передаем его
                       на производство</span></div>

                </div>
            </div>
            <div class="item">
                <div class="step">
                    <div class="num"><span>03</span><img src="img/step3-img.png"></div>
                    <div class="info"><span>Мы готовим ваш комплект еды
                       и раскладываем по порциям</span></div>

                </div>
            </div>
            <div class="item">
                <div class="step">
                    <div class="num"><span>04</span><img src="img/step4-img.png"></div>
                    <div class="info">
                       <span>Курьер доставляет его
                       к вам домой или в офис</span></div>

                </div>
            </div>


        </div>


        <h5 class="text-center">Вы питаетесь правильно и вкусно, достигая своих целей.</h5>

    </div>
</div>
