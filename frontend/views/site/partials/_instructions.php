<?php
if(count($kitchens)) { ?>
<div class="team">
    <div class="container-fluid">
        <h2 class="text-center heading">Знакомьтесь - наша кухня</h2>
    </div>
    <div class="kitchen">
        <div class="owl-carousel owl-theme">
            <?php
            for($i = 0; $i < count($kitchens); $i++)
            {
                echo '<div class="item">';
                $image = Yii::$app->urlManagerBackend->baseUrl . '/uploads/kitchens/' . $kitchens[$i]['image'];
                echo '<img src="' . $image . '">';
                echo '</div>';
            }
            ?>
        </div>
    </div>
</div>
<?php } ?>
