<!-- Modal BASKET -->
<div class="modal fade" id="modalBasket" tabindex="-1" role="dialog" aria-labelledby="Корзина" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form action="/amo/order" method="POST">
                <div class="container-fluid">
                    <button type="button" class="close" data-dismiss="modal" style="font-size: 60px" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="content">
                        <h2 class="text-center heading">Оформление заказа</h2>
                        <div class="items">
                            <input type="hidden" name="delivery_date" :value="nearDate[0] | russianDate">
                            <div class="item" v-for="(food, index) in basket">
                                <input type="hidden" :name="'tarif[' + index + '][]'" :value="food[1]">
                                <input type="hidden" :name="'tarif[' + index + '][]'" :value="food[4]">
                                <input type="hidden" :name="'tarif[' + index + '][]'" :value="food[3]">
                                <input type="hidden" :name="'tarif[' + index + '][]'" :value="food[5]">
                                <input type="hidden" :name="'tarif[' + index + '][]'" :value="food[2]">
                                <div class="item-shop">
                                    <div class="image">
                                        <?php
                                        $image_base_food = Yii::$app->urlManagerBackend->baseUrl . '/uploads/tarifs_source/';
                                        ?>
                                        <img :src="'<?=$image_base_food?>' + food[0]">
                                    </div>
                                    <div class="i">
                                        <div class="type">Наименование</div>
                                        <div class="value" style="letter-spacing: 0.06em;">{{food[1]}}</div>
                                    </div>
                                    <div class="i">
                                        <div class="type">Ккал</div>
                                        <div class="value">{{food[2]}} ккал</div>
                                    </div>
                                    <div class="i">
                                        <div class="type">Дни</div>
                                        <div class="value">{{food[4]}}</div>
                                    </div>
                                    <div class="i price">
                                        <div class="type">Цена</div>
                                        <div class="value"><span>{{food[5]}}</span> <?=$money_symbol?></div>
                                        <div style="display: flex;align-items: center;position: relative;top:-5px;color:#959597;font-weight: 300;font-size: 14px;font-style: italic;">
                                            <span>В день: {{food[3]}}</span><?=$money_symbol_small?></div>
                                    </div>
                                    <div style="flex-basis: 50px;flex-shrink: 0;flex-grow: 0">
                                        <img @click.prevent="removeItem(index)" style="cursor: pointer" src="img/Крестик.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="content-bottom">

                        <div class="total-sum">
                            <span style="color:#959597;font-size:20px;font-weight: 300">Сумма к оплате:
                                <span class="sum">
                                    <span>{{basketSumm}}</span>
                                    <?=$money_symbol?>
                                </span>
                            </span>
                        </div>
                        <h2 class="text-center heading" style="margin-bottom: 17px;margin-top: 23px;letter-spacing: 0.05em;">
                            Заполните форму доставки</h2>
                        <div class="text-right" style="font-size: 14px;margin-bottom: 24px;letter-spacing: 0.05em;">
                            <span class="main-fcolor">*</span> - обязательно для заполнения
                        </div>
                        <div class="order-form">
                            <span class="field">ФИО получателя:</span>
                            <div class="flex-input-form">
                                <input name="name" placeholder="ФИО*" type="text" required>
                            </div>
                            <br>
                            <span class="field">Контакты получателя:</span>
                            <div class="flex-input-form">
                                <input placeholder="Контактный телефон*" type="tel" required name="phone">
                                <input name="email" placeholder="Ваш e-mail" type="text">
                            </div>
                            <br>
                            <span class="field">Адрес доставки:</span>
                            <div class="flex-input-form">
                                <input name="address" placeholder="Например: <?php if($placeholder_destination) echo $placeholder_destination; else echo 'г. Москва, ул. Дальневосточная, д. 252, кв 456';?>" type="text">
                                <div class="shiping-info">
                                    <span>Доставка Вашего заказа</span> <br><span>будет произведена:</span><br>
                                    <div class="time-info">
                                        {{nearDate[0] | russianDate}}<br>
                                        с {{nearDate[0] | onlyTime }} до {{nearDate[1] | onlyTime }}
                                    </div>
                                </div>
                            </div>

                            <!--<div style="margin-bottom: 0px">
                                <input class="comment-flex" placeholder="Комментарий к заказу" type="text">
                            </div>-->
                            <!--<div class="oplata-method">
                                <span class="field" style="flex-shrink: 0;">Способ оплаты:</span>
                                <div class="flex-oplata">
                                    <div>
                                        <input name="shiping" checked="checked" class="radiobtn-basket" type="radio"> Онлайн сейчас
                                    </div>
                                    <div>
                                        <input name="shiping" class="radiobtn-basket" type="radio"> При получении курьеру
                                    </div>
                                </div>
                                <div class="total-sum" style="display: inline-block;top: 5px;">
                                    <span style="color:#959597;font-size:20px;font-weight: 300">Сумма к оплате:
                                        <span class="sum">14500
                                            <i class="fas fa-ruble-sign"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>-->
                        </div>
                        <div style="text-align: center">
                            <a href="#" @click.prevent="buyOrder($event)" class="buy">Оставить заявку</a>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal BASKET -->

<div class="modal fade" id="modalCallback" tabindex="-1" role="dialog" aria-labelledby="Обратный звонок"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form action="/amo/call" method="POST">
                    <h4>Заполните форму</h4>
                    <h5>Мы Вам перезвоним</h5>
                    <input name="name" type="text" placeholder="Ваше имя">
                    <input name="phone" type="tel" placeholder="Номер телефона">
                    <a href="#" @click.prevent="callClick($event)">Заказать звонок</a>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalThanks" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h4>Спасибо за заявку!</h4>
                <h5>Наш менеджер свяжется с Вами.</h5>
                <a href="#" data-dismiss="modal">Закрыть</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalContact" tabindex="-1" role="dialog" aria-labelledby="Контакты" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="phone">
                    <div class="fs-12 fw-t"><?=$city_name?></div>
                    <div><a class="phone" href="tel:<?=$phone?>"><?=$phone?></a></div>
                    <div class="fs-12 fw-t"><?=$time_text?></div>
                </div>
                <?php if($common_phone) { ?>
                    <div class="phone">
                        <div class="fs-12 fw-t"><?=$common_phone['country']?></div>
                        <div><a class="phone" href="tel:<?=$common_phone['number']?>"><?=$common_phone['number']?></a></div>
                        <div class="fs-12 fw-t"><?=$common_phone['time']?></div>
                    </div>
                <?php } ?>
                <div class="call-order py-1 px-3 main-bgcolor bor-r10">
                    <a data-dismiss="modal" data-toggle="modal" data-target="#modalCallback" class="fs-14 fw-t " href="#">Заказать звонок</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalcity" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form action="/amo/call" method="POST">
                    <h4>Выберите город</h4>
                    <?php
                    for ($i = 0; $i < count($city_list); $i++) {
                    ?>
                    <a href="//<?=$city_list[$i]['domen'] . '.' . $base_domen?>" class="city__item <?=$city_list[$i]['domen']==$city['domen']?'active':''?>"><?=$city_list[$i]['name']?></a>
                    <?php
                    }
                    ?>
                </form>
            </div>
        </div>
    </div>
</div>