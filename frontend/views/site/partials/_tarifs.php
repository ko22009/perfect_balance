<div id="tarifs">
    <div class="select-menu grey_block">
        <div class="container-fluid">
            <h2 class="text-center heading">Выберите свой рацион</h2>
            <div class="menus-wrap">
                <div class="wrap">
                    <nav>
                        <div class="nav nav-tabs justify-content-center" id="nav-tab" role="tablist">
                            <a v-for="(section, section_index) in sections" :class="{active: section_active == section_index}" @click="changeSection(section_index, section['id'])" class="nav-item nav-link"><span>{{section['name']}}</span></a>
                        </div>
                    </nav>
                </div>
            <div class="tab-content" id="nav-tabContent">
                <?php
                $image_base = Yii::$app->urlManagerBackend->baseUrl . '/uploads/tarifs_source/';
                ?>
                <div v-for="(tarif, tab_index) in getTarifsSection" v-if="checkTarifs(tab_index)">
                    <div class="tab-pane fade show active" :id="'tarif_' + (tab_index + 1)">
                        <div class="menu-wrap">
                            <div class="menu">
                                <div class="image-wrap mob" :style="'background-image: url(<?=$image_base?>' + tarif.image + ')'">
                                    <div>{{ tarif.name }}</div>
                                </div>
                                <div>
                                    <div class="image-wrap" :style="'background-image: url(<?=$image_base?>' + tarif.image + ')'">
                                        <div>{{ tarif.name }}</div>
                                    </div>
                                    <div class="content">
                                        <div class="setting">
                                            <div class="name">Рассчитайте стоимость</div>
                                            <div class="callories">
                                                <div class="btn-group btn-group-toggle">
                                                    <label v-for="(tarific, index) in tarif.tarifs" data-toggle="buttons" class="btn btn-secondary bor-r10" @click="changeTab(tab_index, index)" :class="{active: tarif.checked == index}">
                                                        <input type="radio" name="options" :id="'option_' + (index + 1)" autocomplete="off">
                                                        <span>{{tarific.tarif_num}}</span> <span>ккал</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="number-days">
                                                <span class="text">Количество дней: </span>
                                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                    <label v-for="(item, index) in tarif['days']" @click="changeNumDay(tab_index, index)" :class="{active: tarif.num_day == index}" class="btn btn-secondary">
                                                    <input type="radio" name="options" id="'option_' + (index + 1)" autocomplete="off"> <span>{{showDay(tarifs[tab_index]['days'], index)}}</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="day-more">
                                                <div class="text">Дневной рацион: {{countFood(tab_index)}}</div>
                                                <ul>
                                                    <li v-for="(food, index) in currentFood(tab_index)">
                                                        <span class="count">{{food.calories}} ккал</span>
                                                        <svg style="vertical-align: middle"
                                                             xmlns="http://www.w3.org/2000/svg"
                                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                                             width="30px" height="1px">
                                                            <path fill-rule="evenodd" fill="rgb(149, 149, 151)"
                                                                  d="M-0.000,0.999 L-0.000,-0.001 L30.000,-0.001 L30.000,0.999 L-0.000,0.999 Z"/>
                                                        </svg>
                                                        <span class="type">{{food.name}}</span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="price">
                                                <div>
                                                    <div class="num">
                                                        <span>{{moneySumm(tab_index)}}</span>
                                                        <?=$money_symbol?>
                                                    </div>
                                                    <div class="inday">- День: <span>
                                                            {{moneyDay(tab_index)}}</span>
                                                        <?=$money_symbol_small?>
                                                    </div>
                                                </div>
                                                <div class="add-to-cart main-bgcolor btn-click">
                                                    <a href="#" @click.prevent="addToBasket(tab_index, $event)"><img src="img/icon_basket.png"><span>В корзину</span> </a>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="view"></div>
                                    </div>
                                    <div class="content-calendar">
                                        <div class="calendar">
                                            <ul class="nav nav-tabs">
                                                <li class="nav-item" v-for="(tarific, index) in tarifs_date" v-if="removeNextDays(tarif.current_page, index)">
                                                    <div href="#" @click="changeDay(tab_index, index)" class="nav-link" :class="{active:index == tarif.active_day}">{{index | onlyDM}}</div>
                                                </li>
                                            </ul>
                                            <div class="tab-content day" id="myTabContent">
                                                <div class="tab-pane fade show active" role="tabpanel"
                                                     aria-labelledby="home-tab">
                                                    <?php
                                                    $image_base_food = Yii::$app->urlManagerBackend->baseUrl . '/uploads/food/' . $domain . '/';
                                                    ?>
                                                    <div v-for="(food, index) in currentFood(tab_index)">
                                                        <img :src="'<?=$image_base_food?>' + $options.filters.onlyYear(tarif.active_day) + '/' + $options.filters.onlyMonth(tarif.active_day) + '/' + $options.filters.onlyDay(tarif.active_day) + '/' + (food.index + 1)+ '.jpg'">
                                                        <div class="hover">
                                                            <p class="r-l mb-10 mt-20">{{food.name}}</p>
                                                            <p class="r-t">Ккал - {{food.calories}} г</p>
                                                            <p class="r-t">Белок - {{food.proteins}} г</p>
                                                            <p class="r-t">Жиры - {{food.fats}} г</p>
                                                            <p class="r-t">Углеводы - {{food.carbohydrates}} г</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <span class="mobile-week-text">Выбор недели</span>
                                            <div class="btn-wrap week-btn">
                                                <div class="prev-week" :class="tarif.prevVisible">
                                                    <a @click.prevent="changePageMinus(tab_index, $event)" style="font-size: 14px;" href="#">
                                                        <span class="arrow_slide left"></span>
                                                        <span class="week-text">Пред. неделя</span>
                                                    </a>
                                                </div>
                                                <div class="mid">
                                                    <a href="#" @click.prevent="changePage(tab_index, page)" v-for="page in pages" :class="{active: page == tarif.current_page}">
                                                    </a>
                                                </div>
                                                <div class="next-week" :class="tarif.nextVisible">
                                                    <a @click.prevent="changePagePlus(tab_index, $event)" style="font-size: 14px;" class="" href="#">
                                                        <span class="week-text">След. неделя</span>
                                                        <span class="arrow_slide right"></span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
