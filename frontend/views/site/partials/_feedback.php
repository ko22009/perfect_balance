<?php
if (count($reviews) > 0) {
    ?>
    <div class="feedback grey_block">
        <div class="container-fluid">
            <h2 class="text-center heading">Мнение клиентов о нашей кухне</h2>
            <div class="wrap">
                <div class="owl-carousel owl-theme">

                    <?php
                    for ($i = 0; $i < count($reviews); $i++) {
                        $before_image = Yii::$app->urlManagerBackend->baseUrl . '/uploads/reviews/' . $reviews[$i]['before_image'];
                        $after_image = Yii::$app->urlManagerBackend->baseUrl . '/uploads/reviews/' . $reviews[$i]['after_image'];
                        ?>

                        <div class="item">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="img-compare-wrap">
                                            <div <?php if ($reviews[$i]['after_image'])
                                                echo 'class="img_compare"' ?>>
                                                <img src="<?= $before_image ?>" alt="Before">
                                                <? if ($reviews[$i]['after_image']) { ?><img src="<?= $after_image ?>"
                                                                              alt="After"> <? } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 align-content-center">
                                        <div class="about">
                                            <div class="about-wrap">
                                                <div class="name">
                                                    <?= $reviews[$i]['name'] ?>
                                                </div>
                                                <div class="add-info">
                                                    <?= $reviews[$i]['description'] ?>
                                                </div>
                                                <div class="type">
                                                    Тариф: <span class="main-fcolor"><?= $reviews[$i]['terif'] ?></span>
                                                </div>
                                                <div class="result">
                                                    Результат: <span><?= $reviews[$i]['result'] ?></span>
                                                </div>
                                                <p>
                                                    <?= nl2br($reviews[$i]['text']) ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>