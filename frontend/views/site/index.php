<?php
/* @var $this yii\web\View */
$this->title = 'Perfect Balance - ' . $city['name'];
$city_name = $city['name'];
$phone = $contacts['number'];
$time_text = $contacts['time_text'];
$hostname = \Yii::$app->request->hostName;
$base_domain = \Yii::$app->params['base_domain'];
if(isset(\Yii::$app->params['money_symbol'])) {
    $money_symbol_small = $money_symbol = '<span style="margin-left: 5px;font-size: 80%;    display: inline-flex;
    justify-content: center;
    align-items: center;">' . \Yii::$app->params['money_symbol'] . '</span>';
    $money_symbol_text = \Yii::$app->params['money_symbol'];
} else {
    $money_symbol = '<img style="margin-left: 3px" src="img/rub.png">';
    $money_symbol_small = '<img style="margin-left: 3px" src="img/rub-small.png">';
    $money_symbol_text = 'руб';
}

$common_phone = [];

if(isset(\Yii::$app->params['common_phone_country']))
$common_phone['country'] = \Yii::$app->params['common_phone_country'];

if(isset(\Yii::$app->params['common_phone_number']))
$common_phone['number'] = \Yii::$app->params['common_phone_number'];

if(isset(\Yii::$app->params['common_phone_time']))
$common_phone['time'] = \Yii::$app->params['common_phone_time'];

preg_match('/(\w+)\.' . preg_quote($base_domain) . '/', $hostname, $matches);
$domain = isset($matches[1])?$matches[1]:\Yii::$app->request->hostName;

if(isset(\Yii::$app->params['location_by_ip']))
    $by_ip = \Yii::$app->params['location_by_ip'];
else $by_ip = null;
?>

<?php
$is_bot = preg_match(
    "~(Google|Yahoo|Rambler|Bot|Yandex|Spider|Snoopy|Crawler|Finder|Mail|curl)~i",
    $_SERVER['HTTP_USER_AGENT']
);

if(!$is_bot) {
    if ($by_ip) {
        if (empty($matches)) {
            function get_client_ip() {
                $ipaddress = '';
                if (isset($_SERVER['HTTP_CLIENT_IP']))
                    $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
                else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
                else if (isset($_SERVER['HTTP_X_FORWARDED']))
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
                else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
                    $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
                else if (isset($_SERVER['HTTP_FORWARDED']))
                    $ipaddress = $_SERVER['HTTP_FORWARDED'];
                else if (isset($_SERVER['REMOTE_ADDR']))
                    $ipaddress = $_SERVER['REMOTE_ADDR'];
                else
                    $ipaddress = 'UNKNOWN';
                return $ipaddress;
            }

            $ip = get_client_ip();
            $response = json_decode(file_get_contents('http://api.sypexgeo.net/json/' . $ip));
            if ($response->city) {
                $lat = $response->city->lat;
                $lon = $response->city->lon;

                $js = <<< JS
            $.post("/cities", {
            latitude: $lat,
            longitude: $lon
            }, function (data) {
                window.location.href = data
            });
JS;
                $this->registerJs($js, \yii\web\View::POS_LOAD);
            }
        }
    } else { ?>
        <script src="http://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU"></script>
        <script>
            /**
             * Только на основной странице, не срабатывает на поддоменах. Там автоматом город ставится
             */
            if (window.location.href.match(/\/\/<?=$base_domain?>/g)) {
                ymaps.ready(init)
                function init() {
                    var name = ymaps.geolocation.city
                    changeCity(name)
                }
            }
            function changeCity(name) {
                $.get("//geocode-maps.yandex.ru/1.x/?format=json&geocode=" + name, function (data) {
                    var pos = data['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos']
                    pos = pos.split(' ')
                    var longitude = pos[0]
                    var latitude = pos[1]
                    $.post("/cities", {latitude: latitude, longitude: longitude}, function (data) {
                        window.location.href = data
                    });
                });
            }
        </script>
    <?php }
} ?>
<div id="app">
<?= $this->render('partials/_header', ['city_name' => $city_name, 'phone' => $phone, 'time_text' => $time_text, 'contacts' => $contacts, 'common_phone' => $common_phone, 'money_symbol_text' => $money_symbol_text]); ?>

<?= $this->render('partials/_slider', ['slider' => $slider]); ?>
<?= $this->render('partials/_good_eat_block'); ?>
<?= $this->render('partials/_tarifs', ['domain' => $domain, 'money_symbol' => $money_symbol, 'money_symbol_small' => $money_symbol_small]); ?>
<?= $this->render('partials/_workers', ['crew' => $crew]); ?>
<?= $this->render('partials/_instructions', ['kitchens' => $kitchens]); ?>
<?= $this->render('partials/_feedback', ['reviews' => $reviews]); ?>
<?= $this->render('partials/_partners', ['partners' => $partners]); ?>

<?= $this->render('partials/_footer'); ?>
<?= $this->render('partials/_modals', ['city_name' => $city_name, 'phone' => $phone, 'time_text' => $time_text, 'city_list' => $city_list, 'city' => $city, 'base_domen'=> $base_domain, 'placeholder_destination' => $city['placeholder_destination'], 'money_symbol' => $money_symbol, 'money_symbol_small' => $money_symbol_small, 'common_phone' => $common_phone]); ?>
</div>