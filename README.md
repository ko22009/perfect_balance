IHOR folder permisson and files.

find . -type d -exec chmod 755 {} \; 
find . -type f -exec chmod 644 {} \;

Для того, чтобы в папку загрузки на backend/web загружалось, надо дать права
chown -R www-data backend/uploads

Для правильного определения домена и работы всего сайта
В файле common/config/params-local.php
нужно указать стандартный домен, либо perfectbalance.com.ua, либо другой.
'base_domain' => 'perfectbalance.ru'

Для правильной переадресации в common/config/main-local.php в разделе components.
Указывается определенный порт, т.к. у нас есть поддомены для городов. Например: msc.perfectbalance.ru
Админка на порт - идеальное решение для поддоменов.
'urlManagerBackend' => [
    'class' => 'yii\web\urlManager',
    'baseUrl' => '//perfectbalance.ru:3000',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
],

Для отправки заявок в amocrm
В файле frontend/config/main-local.php в разделе components
'amocrm' => [
    'class' => 'yii\amocrm\Client',
    'subdomain' => 'perfectbalance',
    'login' => 'mail@mail.ru',
    'hash' => 'some_hash'
],

Картинки для блюд заливаются через файловый менеджер. Например. Где mgn - поддомен, город
backend/web/uploads/food/mgn/2018/04/23/1.jpg

Для телефона маска ввода в файле common/config/params-local.php
'mask_phone' => '+38(099)9999999'

Для символа денег в файле common/config/params-local.php
'money_symbol' => ' Грн'

Для общего телефона по стране в файле frontend/config/params-local.php
'common_phone_country' => 'По России',
'common_phone_number' => '8 (800) 422-95-52',
'common_phone_time' => 'с 9:00 до 22:00 по Мск времени'

Для определения города по ip frontend/config/params-local.php
'location_by_ip' => true