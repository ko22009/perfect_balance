<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tarifs`.
 */
class m180407_165823_create_tarifs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tarifs', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->notNull(),
            'tarif_id' => $this->integer()->notNull(),
            'tarif_num' => $this->integer(),
            'food_1' => $this->float(),
            'food_2' => $this->float(),
            'food_3' => $this->float(),
            'food_4' => $this->float(),
            'food_5' => $this->float(),
            'food_6' => $this->float(),
            'money' => $this->text()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tarifs');
    }
}
