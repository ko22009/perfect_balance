<?php

use yii\db\Migration;

/**
 * Handles the creation of table `kitchens`.
 */
class m180406_160910_create_kitchens_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('kitchens', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->notNull(),
            'image' => $this->text()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('kitchens');
    }
}
