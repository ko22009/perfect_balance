<?php

use yii\db\Migration;

/**
 * Handles the creation of table `slides`.
 */
class m180406_160819_create_slides_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('slides', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->notNull(),
            'theme_id' => $this->integer()->notNull(),
            'title' => $this->text(),
            'subtitle' => $this->text(),
            'discount' => $this->text(),
            'link' => $this->integer(),
            'nav_title' => $this->text(),
            'image' => $this->text()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('slides');
    }
}
