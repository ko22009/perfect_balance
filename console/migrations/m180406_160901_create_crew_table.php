<?php

use yii\db\Migration;

/**
 * Handles the creation of table `crew`.
 */
class m180406_160901_create_crew_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('crew', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->notNull(),
            'name' => $this->string(64),
            'position' => $this->string(64),
            'quote' => $this->text(),
            'image' => $this->text()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('crew');
    }
}
