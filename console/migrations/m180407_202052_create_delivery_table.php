<?php

use yii\db\Migration;

/**
 * Handles the creation of table `delivery`.
 */
class m180407_202052_create_delivery_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('delivery', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->notNull(),
            'week_number' => $this->integer(),
            'time' => $this->time(),
            'time2' => $this->time(),
            'week_number2' => $this->integer(),
            'time3' => $this->time(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('delivery');
    }
}
