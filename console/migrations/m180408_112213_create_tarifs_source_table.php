<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tarifs_source`.
 */
class m180408_112213_create_tarifs_source_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tarifs_source', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->notNull(),
            'days' => $this->text(),
            'name' => $this->string(128),
            'order' => $this->integer()->notNull(),
            'image' => $this->text()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tarifs_source');
    }
}
