<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tarifs_date`.
 */
class m180407_172245_create_tarifs_date_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tarifs_date', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->notNull(),
            'date' => $this->date(),
            'name' => $this->string(128),
            'calories' => $this->integer(),
            'proteins' => $this->integer(),
            'fats' => $this->integer(),
            'carbohydrates' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tarifs_date');
    }
}
