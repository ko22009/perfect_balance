<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tarifs_sections`.
 */
class m180423_001849_create_tarifs_sections_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tarifs_sections', [
            'id' => $this->primaryKey(),
            'name' => $this->string(64),
            'tarifs' => $this->text(),
            'city_id' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tarifs_sections');
    }
}
