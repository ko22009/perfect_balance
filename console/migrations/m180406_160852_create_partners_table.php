<?php

use yii\db\Migration;

/**
 * Handles the creation of table `partners`.
 */
class m180406_160852_create_partners_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('partners', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->notNull(),
            'image' => $this->text(),
            'link' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('partners');
    }
}
