<?php

use yii\db\Migration;

/**
 * Handles the creation of table `type_themes`.
 */
class m180406_160809_create_type_themes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('type_themes', [
            'id' => $this->primaryKey(),
            'name' => $this->string(64),
            'indicator' => $this->integer()->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('type_themes');
    }
}
