<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contacts`.
 */
class m180406_160831_create_contacts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('contacts', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->notNull(),
            'number' => $this->string(18),
            'vk' => $this->string(128),
            'instagram' => $this->string(128),
            'fb' => $this->string(128),
            'time_text' => $this->string(64),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('contacts');
    }
}
