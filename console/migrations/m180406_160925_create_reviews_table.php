<?php

use yii\db\Migration;

/**
 * Handles the creation of table `reviews`.
 */
class m180406_160925_create_reviews_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('reviews', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->notNull(),
            'name' => $this->string(64),
            'description' => $this->string(64),
            'terif' => $this->string(64),
            'result' => $this->string(64),
            'text' => $this->text(),
            'before_image' => $this->text(),
            'after_image' => $this->text()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('reviews');
    }
}
