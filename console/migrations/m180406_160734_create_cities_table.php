<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cities`.
 */
class m180406_160734_create_cities_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('cities', [
            'id' => $this->primaryKey(),
            'name' => $this->string(64),
            'domen' => $this->string(64),
            'latitude' => $this->float(),
            'longitude' => $this->float(),
            'google_id' => $this->string(64),
            'yandex_id' => $this->string(64),
            'fb_pixel_id' => $this->string(64),
            'vk_pixel_id' => $this->string(64),
            'vk_pixel_view' => $this->string(64),
            'vk_pixel_madeOrder' => $this->string(64),
            'vk_pixel_call' => $this->string(64),
            'user_id_amo_crm' => $this->string(12),
            'placeholder_destination' => $this->string(128),
            'down' => $this->boolean(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('cities');
    }
}
