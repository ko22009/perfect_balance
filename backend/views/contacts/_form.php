<?php

use app\models\Cities;
use backend\controllers\GlobalController;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Contacts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contacts-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if (\Yii::$app->user->identity->isCityShow()) { ?>

        <?= $form->field($model, 'city_id')->widget(Select2::class, [
            'data' => ArrayHelper::map(Cities::find()->all(), 'id', 'name'),
            'options' => ['placeholder' => 'Select a city ...']
        ]); ?>

    <?php } else {
        $model->city_id = GlobalController::getCityID();
        echo $form->field($model, 'city_id')->hiddenInput()->label(false);
    } ?>

    <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'time_text')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vk')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'instagram')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'fb')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
