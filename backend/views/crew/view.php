<?php

use app\models\Cities;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Crew */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Crews', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crew-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'label'  => 'city_id',
                'value'  => Cities::findOne(['id' => $model->city_id])['name']
            ],
            'name',
            'position',
            'quote:ntext',
            [
                'label'  => 'image',
                'format' => 'raw',
                'value'  => $model->image?'<img style="max-width: 100%" src="/uploads/crews/' . $model->image . '" />':''
            ],
        ],
    ]) ?>

</div>
