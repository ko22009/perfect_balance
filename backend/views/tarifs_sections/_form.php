<?php

use app\models\Cities;
use backend\controllers\GlobalController;
use kartik\grid\GridView;
use kartik\select2\Select2;
use unclead\multipleinput\MultipleInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TarifsSections */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tarifs-sections-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if (\Yii::$app->user->identity->isCityShow()) { ?>
        <?= $form->field($model, 'city_id')->widget(Select2::class, [
            'data' => ArrayHelper::map(Cities::find()->all(), 'id', 'name'),
            'options' => ['placeholder' => 'Select a city ...'],
            'id' => 'city_id'
        ]); ?>

    <?php } else {
        $model->city_id = GlobalController::getCityID();
        echo $form->field($model, 'city_id')->hiddenInput()->label(false);
    } ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php
    echo $form->field($model, 'tarifs')->widget(MultipleInput::class, [
        'id' => 'tarifs_section_tarif',
        'columns' => [
            [
                'name' => 'tarif_id',
                'type' => GridView::FILTER_SELECT2,
                'title' => 'Tarif'
            ],
        ]
    ])->label(false);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
