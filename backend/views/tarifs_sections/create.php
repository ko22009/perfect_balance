<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TarifsSections */

$this->title = 'Create Tarifs Sections';
$this->params['breadcrumbs'][] = ['label' => 'Tarifs Sections', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tarifs-sections-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
