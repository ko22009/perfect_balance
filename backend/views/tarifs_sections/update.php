<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TarifsSections */

$this->title = 'Update Tarifs Sections:';
$this->params['breadcrumbs'][] = ['label' => 'Tarifs Sections', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tarifs-sections-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
