<?php

use app\models\Cities;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'city_id')->widget(Select2::class, [
        'data' => ArrayHelper::map(Cities::find()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Select a city ...', 'allowClear' => true],
        'pluginOptions' => ['allowClear' => true]
    ]); ?>

    <?= $form->field($model, 'active')->checkbox(); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
