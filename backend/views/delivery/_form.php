<?php

use app\models\Cities;
use backend\controllers\GlobalController;
use kartik\time\TimePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Delivery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="delivery-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if (\Yii::$app->user->identity->isCityShow()) { ?>

        <?= $form->field($model, 'city_id')->widget(Select2::class, [
            'data' => ArrayHelper::map(Cities::find()->all(), 'id', 'name'),
            'options' => ['placeholder' => 'Select a city ...']
        ]); ?>

    <?php } else {
        $model->city_id = GlobalController::getCityID();
        echo $form->field($model, 'city_id')->hiddenInput()->label(false);
    } ?>

    <?= $form->field($model, 'week_number')->widget(Select2::class, [
        'data' => [
            '1' => 'Понедельник',
            '2' => 'Вторник',
            '3' => 'Среда',
            '4' => 'Четверг',
            '5' => 'Пятница',
            '6' => 'Субботу',
            '7' => 'Воскресенье',
        ],
        'options' => ['placeholder' => 'Select a week ...']
    ]) ?>

    <?= $form->field($model, 'time')->widget(TimePicker::class, [
        'pluginOptions' => [
            'showMeridian' => false
        ]
    ]); ?>

    <?= $form->field($model, 'time2')->widget(TimePicker::class, [
        'pluginOptions' => [
            'showMeridian' => false
        ]
    ]); ?>

    <?= $form->field($model, 'week_number2')->widget(Select2::class, [
        'data' => [
            '1' => 'Понедельник',
            '2' => 'Вторник',
            '3' => 'Среда',
            '4' => 'Четверг',
            '5' => 'Пятница',
            '6' => 'Субботу',
            '7' => 'Воскресенье',
        ],
        'options' => ['placeholder' => 'Select a week ...']
    ]) ?>

    <?= $form->field($model, 'time3')->widget(TimePicker::class, [
        'pluginOptions' => [
            'showMeridian' => false
        ]
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
