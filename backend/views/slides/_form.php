<?php

use app\models\Cities;
use backend\controllers\GlobalController;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\Type_themes;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Slides */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="slides-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if (\Yii::$app->user->identity->isCityShow()) { ?>

        <?= $form->field($model, 'city_id')->widget(Select2::class, [
            'data' => ArrayHelper::map(Cities::find()->all(), 'id', 'name'),
            'options' => ['placeholder' => 'Select a city ...']
        ]); ?>

    <?php } else {
        $model->city_id = GlobalController::getCityID();
        echo $form->field($model, 'city_id')->hiddenInput()->label(false);
    } ?>

    <?= $form->field($model, 'theme_id')->widget(Select2::class, [
        'data' => ArrayHelper::map(Type_themes::find()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Select a theme ...']
    ]); ?>

    <?= $form->field($model, 'title')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'subtitle')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'discount')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'link')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'nav_title')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'imageFile')->widget(FileInput::class, [
        'options' => ['accept' => 'image/*'],
        'pluginOptions' => [
            'allowedFileExtensions' => ['jpg', 'gif', 'png'],
            'showUpload' => false,
            'initialPreview' => [
                $model->image != null ? '<img src="/uploads/slides/' . $model->image . '" class="file-preview-image">' : ''
            ],
            'initialCaption' => $model->image != null ? $model->image : '',
            'fileActionSettings' => [
                'showDrag' => false,
                'showZoom' => true,
                'showRemove' => false
            ]
            ]
        ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
