<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Slides */
/* @var $modelUpload app\models\UploadForm */

$this->title = 'Update Slides:';
$this->params['breadcrumbs'][] = ['label' => 'Slides', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="slides-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>
