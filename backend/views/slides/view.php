<?php

use app\models\Cities;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Slides */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Slides', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slides-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'label'  => 'city_id',
                'value'  => Cities::findOne(['id' => $model->city_id])['name']
            ],
            'theme_id',
            'title:ntext',
            'subtitle:ntext',
            'discount',
            'link:ntext',
            'nav_title:ntext',
            [
                'label'  => 'image',
                'format' => 'raw',
                'value'  => $model->image?'<img style="max-width: 100%" src="/uploads/slides/' . $model->image . '" />':''
            ],
        ],
    ]) ?>

</div>
