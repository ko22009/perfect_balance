<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TarifsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tarifs-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'tarif_name') ?>

    <?= $form->field($model, 'tarif_num') ?>

    <?= $form->field($model, 'food_1') ?>

    <?= $form->field($model, 'food_2') ?>

    <?php // echo $form->field($model, 'food_3') ?>

    <?php // echo $form->field($model, 'food_4') ?>

    <?php // echo $form->field($model, 'food_5') ?>

    <?php // echo $form->field($model, 'food_6') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
