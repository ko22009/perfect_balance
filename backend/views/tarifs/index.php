<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TarifsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tarifs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tarifs-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tarifs', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'city_id',
                'value'     => 'cities.name'
            ],
            'tarif_id',
            'tarif_num',
            'food_1',
            'food_2',
            'food_3',
            'food_4',
            'food_5',
            'food_6',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
