<?php

use app\models\Cities;
use backend\controllers\GlobalController;
use common\models\Tarifs_source;
use kartik\grid\GridView;
use kartik\select2\Select2;
use unclead\multipleinput\MultipleInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Tarifs */
/* @var $form yii\widgets\ActiveForm */

if (!\Yii::$app->user->identity->isCityShow())
    $class_hide = 'hide_selector';
else $class_hide = '';
?>

<div class="tarifs-form">

    <?php $form = ActiveForm::begin(['options' => ['class' => $class_hide]]); ?>

    <?php if (\Yii::$app->user->identity->isCityShow()) { ?>

        <?= $form->field($model, 'city_id')->widget(Select2::class, [
            'data' => ArrayHelper::map(Cities::find()->all(), 'id', 'name'),
            'options' => ['placeholder' => 'Select a city ...'],
            'id' => 'city_id'
        ]); ?>

    <?php } else {
        $model->city_id = GlobalController::getCityID();
        echo $form->field($model, 'city_id')->widget(Select2::class, [
            'data' => ArrayHelper::map(Cities::find()->all(), 'id', 'name'),
            'options' => ['placeholder' => 'Select a city ...'],
            'id' => 'city_id'
        ])->label(false);
    } ?>

    <?= $form->field($model, 'tarif_id')->widget(Select2::class, [
        'options' => ['placeholder' => 'Select a tarif ...'],
        'id' => 'tarif_id'
    ]); ?>

    <?= $form->field($model, 'tarif_num')->textInput() ?>

    <?= $form->field($model, 'food_1')->textInput() ?>

    <?= $form->field($model, 'food_2')->textInput() ?>

    <?= $form->field($model, 'food_3')->textInput() ?>

    <?= $form->field($model, 'food_4')->textInput() ?>

    <?= $form->field($model, 'food_5')->textInput() ?>

    <?= $form->field($model, 'food_6')->textInput() ?>

    <?php
    echo $form->field($model, 'money')->widget(MultipleInput::class, [
        'id' => 'tarifs_money',
        'columns' => [
            [
                'name' => 'day',
                'type' => GridView::FILTER_SELECT2,
                'title' => 'Day',
            ],
            [
                'name' => 'money',
                'title' => 'Money',
                'enableError' => true,
                'options' => [
                    'type' => 'number',
                ],
            ],
        ]
    ])->label(false);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
