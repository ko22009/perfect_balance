<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\KitchensSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kitchens';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kitchens-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Kitchens', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'city_id',
                'value'     => 'cities.name'
            ],
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->image!='')
                        return '<img src="'.Yii::$app->homeUrl. 'uploads/kitchens/'.$model->image.'" style="max-width:100%" height="auto">'; else return 'no image';
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
