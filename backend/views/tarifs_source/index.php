<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\Tarifs_sourceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tarifs Sources';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tarifs-source-index">

    <?= Html::a('Разделы тарифов', ['/tarifs_sections']) ?>
    <?= Html::a('Тарифы', ['/tarifs']) ?>
    <?= Html::a('Тарифы_Дни', ['/tarifs_date']) ?>

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tarifs Source', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'city_id',
                'value'     => 'cities.name'
            ],
            'name',
            'order',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
