<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Tarifs_source */

$this->title = 'Create Tarifs Source';
$this->params['breadcrumbs'][] = ['label' => 'Tarifs Sources', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tarifs-source-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
