<?php

use app\models\Cities;
use backend\controllers\GlobalController;
use kartik\file\FileInput;
use kartik\select2\Select2;
use unclead\multipleinput\MultipleInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Tarifs_source */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tarifs-source-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if (\Yii::$app->user->identity->isCityShow()) { ?>

        <?= $form->field($model, 'city_id')->widget(Select2::class, [
            'data' => ArrayHelper::map(Cities::find()->all(), 'id', 'name'),
            'options' => ['placeholder' => 'Select a city ...']
        ]); ?>

    <?php } else {
        $model->city_id = GlobalController::getCityID();
        echo $form->field($model, 'city_id')->hiddenInput()->label(false);
    } ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'order')->textInput() ?>

    <?= $form->field($model, 'imageFile')->widget(FileInput::class, [
        'options' => ['accept' => 'image/*'],
        'pluginOptions' => [
            'allowedFileExtensions' => ['jpg', 'gif', 'png'],
            'showUpload' => false,
            'initialPreview' => [
                $model->image != null ? '<img src="/uploads/tarifs_source/' . $model->image . '" class="file-preview-image">' : ''
            ],
            'initialCaption' => $model->image != null ? $model->image : '',
            'fileActionSettings' => [
                'showDrag' => false,
                'showZoom' => true,
                'showRemove' => false
            ]
        ]
    ]); ?>

    <?php
    echo $form->field($model, 'days')->widget(MultipleInput::class)->label(false);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
