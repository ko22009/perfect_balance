<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Type_themes */

$this->title = 'Update Type Themes:';
$this->params['breadcrumbs'][] = ['label' => 'Type Themes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="type-themes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
