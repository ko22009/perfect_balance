<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Type_themes */

$this->title = 'Create Type Themes';
$this->params['breadcrumbs'][] = ['label' => 'Type Themes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="type-themes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
