<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Cities */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cities-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'domen')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'google_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'yandex_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fb_pixel_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vk_pixel_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vk_pixel_view')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vk_pixel_madeOrder')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vk_pixel_call')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_id_amo_crm')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'placeholder_destination')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'down')->checkbox(); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
