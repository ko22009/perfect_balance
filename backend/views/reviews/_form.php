<?php

use app\models\Cities;
use backend\controllers\GlobalController;
use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Reviews */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reviews-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if (\Yii::$app->user->identity->isCityShow()) { ?>

        <?= $form->field($model, 'city_id')->widget(Select2::class, [
            'data' => ArrayHelper::map(Cities::find()->all(), 'id', 'name'),
            'options' => ['placeholder' => 'Select a city ...']
        ]); ?>

    <?php } else {
        $model->city_id = GlobalController::getCityID();
        echo $form->field($model, 'city_id')->hiddenInput()->label(false);
    } ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'terif')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'result')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'imageFile1')->widget(FileInput::class, [
        'options' => ['accept' => 'image/*'],
        'pluginOptions' => [
            'allowedFileExtensions' => ['jpg', 'gif', 'png'],
            'showUpload' => false,
            'initialPreview' => [
                $model->before_image != null ? '<img src="/uploads/reviews/' . $model->before_image . '" class="file-preview-image">' : ''
            ],
            'initialCaption' => $model->before_image != null ? $model->before_image : '',
            'fileActionSettings' => [
                'showDrag' => false,
                'showZoom' => true,
                'showRemove' => false
            ]
        ]
    ]); ?>

    <?= $form->field($model, 'imageFile2')->widget(FileInput::class, [
        'options' => ['accept' => 'image/*'],
        'pluginOptions' => [
            'allowedFileExtensions' => ['jpg', 'gif', 'png'],
            'showUpload' => false,
            'initialPreview' => [
                $model->after_image != null ? '<img src="/uploads/reviews/' . $model->after_image . '" class="file-preview-image">' : ''
            ],
            'initialCaption' => $model->after_image != null ? $model->after_image : '',
            'fileActionSettings' => [
                'showDrag' => false,
                'showZoom' => true,
                'showRemove' => false
            ]
        ]
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
