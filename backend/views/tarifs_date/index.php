<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\Tarifs_dateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tarifs Dates';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tarifs-date-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tarifs Date', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Import Tarifs', ['import'], ['class' => 'btn btn-warning']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'city_id',
                'value'     => 'cities.name'
            ],
            'date',
            'name',
            'calories',
            'proteins',
            //'fats',
            //'carbohydrates',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
