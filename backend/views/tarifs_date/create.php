<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TarifsDate */

$this->title = 'Create Tarifs Date';
$this->params['breadcrumbs'][] = ['label' => 'Tarifs Dates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tarifs-date-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
