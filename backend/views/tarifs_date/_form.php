<?php

use app\models\Cities;
use backend\controllers\GlobalController;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Tarifs_date */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tarifs-date-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'date')->widget(DatePicker::class, [
        'language' => 'ru',
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]) ?>

    <?php if (\Yii::$app->user->identity->isCityShow()) { ?>

        <?= $form->field($model, 'city_id')->widget(Select2::class, [
            'data' => ArrayHelper::map(Cities::find()->all(), 'id', 'name'),
            'options' => ['placeholder' => 'Select a city ...']
        ]); ?>

    <?php } else {
        $model->city_id = GlobalController::getCityID();
        echo $form->field($model, 'city_id')->hiddenInput()->label(false);
    } ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'calories')->textInput() ?>

    <?= $form->field($model, 'proteins')->textInput() ?>

    <?= $form->field($model, 'fats')->textInput() ?>

    <?= $form->field($model, 'carbohydrates')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
