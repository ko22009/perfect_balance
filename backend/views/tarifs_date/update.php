<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TarifsDate */

$this->title = 'Update Tarifs Date:';
$this->params['breadcrumbs'][] = ['label' => 'Tarifs Dates', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tarifs-date-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
