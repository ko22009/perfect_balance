<?php
use app\models\Cities;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/**
 * @var $model app\models\Tarifs_date
 */
?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

<?= $form->field($model, 'city_id')->widget(Select2::class, [
    'data' => ArrayHelper::map(Cities::find()->all(), 'id', 'name'),
    'options' => ['placeholder' => 'Select a city ...']
]); ?>

<?= $form->field($model, 'file')->fileInput() ?>

<button>Import</button>

<?php ActiveForm::end() ?>