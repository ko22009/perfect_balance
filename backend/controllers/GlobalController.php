<?php

namespace backend\controllers;

use app\models\Cities;
use Yii;
use yii\web\Controller;

/**
 * CitiesController implements the CRUD actions for Cities model.
 */
class GlobalController extends Controller {

    public function beforeAction($action) {
        $base_domain = \Yii::$app->params['base_domain'];
        $match = preg_match('/(\w+)\.' . preg_quote($base_domain) . '/', \Yii::$app->request->hostName, $matches);
        if ($match) {
            $city = Cities::findOne(['domen' => $matches[1]]);
        } else $city = null;
        if (!\Yii::$app->user->id && \Yii::$app->request->url != '/site/login') return $this->redirect('/site/login');
        if (isset(\Yii::$app->user->identity)) {
            if (!\Yii::$app->user->identity->active) {
                echo $this->render('/site/error', ['name' => 'Аккаунт не активирован', 'message' => 'Попросите главного администратора активировать аккаунт']);
                die();
            }
            $current_city = \Yii::$app->user->identity->cities;
            if ($current_city['id']) {
                if (isset($city['id']) && $city['id'] != $current_city['id'] || $current_city['id'] && !isset($city['id'])) {
                    echo $this->render('/site/error', ['name' => 'Данный аккаунт не работает в этом городе', 'message' => 'Попросите главного администратора поменять город или смените город']);
                    die();
                }
            }
        }
        return parent::beforeAction($action);
    }

    public function isGlobalUser () {
        if(!isset(Yii::$app->user->identity)) {
            return $this->redirect('/site/login');
        } else if(!Yii::$app->user->identity->IsGlobalAdmin) {
            echo $this->render('/site/error', ['name' => 'Запрещено', 'message' => 'Доступ в данный раздел запрещен']);
            die();
        }
    }

    public static function getCityID() {
        $base_domain = \Yii::$app->params['base_domain'];
        $match = preg_match('/(\w+)\.' . preg_quote($base_domain) . '/', \Yii::$app->request->hostName, $matches);
        if ($match) {
            $city_id = Cities::findOne(['domen' => $matches[1]])['id'];
        } else $city_id = null;
        return $city_id;
    }

    public function access($id) {
        $user_city = \Yii::$app->user->identity->cities;
        if($id != $user_city['city_id'] && $user_city['city_id']) {
            echo $this->render('/site/error', ['name' => 'Нет доступа', 'message' => 'У вас нет доступа к этому городу']);
            die();
        }
    }
}
