<?php

namespace backend\controllers;

use Yii;
use app\models\Reviews;
use backend\models\ReviewsSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ReviewsController implements the CRUD actions for Reviews model.
 */
class ReviewsController extends GlobalController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Reviews models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReviewsSearch();
        Yii::$app->request->setQueryParams(array_replace_recursive(Yii::$app->request->getQueryParams(), ["ReviewsSearch" => ["city_id" => GlobalController::getCityID()]]));
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Reviews model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        GlobalController::access($this->findModel($id)['city_id']);
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Reviews model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Reviews();

        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'imageFile1');
            if (!is_null($image)) {
                $model->before_image = $image->name;
                $fileinfo = pathinfo($image->name);
                $ext = $fileinfo['extension'];
                $model->before_image = Yii::$app->security->generateRandomString() . ".{$ext}";
                Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/uploads/reviews/';
                $path = Yii::$app->params['uploadPath'] . $model->before_image;
                $image->saveAs($path);
            }
            $image = UploadedFile::getInstance($model, 'imageFile2');
            if (!is_null($image)) {
                $model->after_image = $image->name;
                $fileinfo = pathinfo($image->name);
                $ext = $fileinfo['extension'];
                $model->after_image = Yii::$app->security->generateRandomString() . ".{$ext}";
                Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/uploads/reviews/';
                $path = Yii::$app->params['uploadPath'] . $model->after_image;
                $image->saveAs($path);
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                var_dump($model->getErrors());
                die();
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Reviews model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        GlobalController::access($this->findModel($id)['city_id']);
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'imageFile1');
            if (!is_null($image)) {
                $model->before_image = $image->name;
                $fileinfo = pathinfo($image->name);
                $ext = $fileinfo['extension'];
                $model->before_image = Yii::$app->security->generateRandomString() . ".{$ext}";
                Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/uploads/reviews/';
                $path = Yii::$app->params['uploadPath'] . $model->before_image;
                $image->saveAs($path);
            }
            $image = UploadedFile::getInstance($model, 'imageFile2');
            if (!is_null($image)) {
                $model->after_image = $image->name;
                $fileinfo = pathinfo($image->name);
                $ext = $fileinfo['extension'];
                $model->after_image = Yii::$app->security->generateRandomString() . ".{$ext}";
                Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/uploads/reviews/';
                $path = Yii::$app->params['uploadPath'] . $model->after_image;
                $image->saveAs($path);
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                var_dump($model->getErrors());
                die();
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Reviews model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        GlobalController::access($this->findModel($id)['city_id']);
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Reviews model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Reviews the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Reviews::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
