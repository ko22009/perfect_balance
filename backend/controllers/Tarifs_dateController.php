<?php

namespace backend\controllers;

use PhpOffice\PhpSpreadsheet\IOFactory;
use Yii;
use app\models\Tarifs_date;
use backend\models\Tarifs_dateSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * TarifsDateController implements the CRUD actions for TarifsDate model.
 */
class Tarifs_dateController extends GlobalController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TarifsDate models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new Tarifs_dateSearch();
        Yii::$app->request->setQueryParams(array_replace_recursive(Yii::$app->request->getQueryParams(), ["Tarifs_dateSearch" => ["city_id" => GlobalController::getCityID()]]));
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TarifsDate model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        GlobalController::access($this->findModel($id)['city_id']);
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TarifsDate model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tarifs_date();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TarifsDate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        GlobalController::access($this->findModel($id)['city_id']);
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TarifsDate model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        GlobalController::access($this->findModel($id)['city_id']);
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TarifsDate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TarifsDate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tarifs_date::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionImport()
    {
        $model = new Tarifs_date();
        if ($model->load(Yii::$app->request->post())) {
            $imageFile = UploadedFile::getInstance($model, 'file');
            Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/uploads/tarifs_date/';
            $path = Yii::$app->params['uploadPath'] . $imageFile->name;
            $imageFile->saveAs($path);
            $filename = pathinfo($imageFile->name)['filename'];
            $date_start = date('Y-m-d', strtotime($filename));
            $reader = IOFactory::createReader('Xlsx');
            $reader->setLoadAllSheets();
            $city_id = $model->city_id;
            $spreadsheet = $reader->load($path);
            for ($i = 0; $i < $spreadsheet->getSheetCount(); $i++) {
                $spreadsheet->setActiveSheetIndex($i);
                $sheet = $spreadsheet->getActiveSheet();
                $highestRow = $sheet->getHighestDataRow();
                $hightestColumn = $sheet->getHighestDataColumn();
                $date = date('Y-m-d', strtotime("+" . $i . " day", strtotime($date_start)));
                for ($row = 2; $row <= $highestRow; $row++) {
                    $rowData = $sheet->rangeToArray('B' . $row . ':' . $hightestColumn . $row, null, true, true);
                    $tarif_data = new Tarifs_date();
                    $tarif_data->date = $date;
                    $tarif_data->name = $rowData[0][0];
                    $tarif_data->calories = $rowData[0][1];
                    $tarif_data->proteins = $rowData[0][2];
                    $tarif_data->fats = $rowData[0][3];
                    $tarif_data->carbohydrates = $rowData[0][4];
                    $tarif_data->city_id = $city_id;
                    $tarif_data->save();
                }
            }
            unlink($path);
            return $this->redirect(['index']);
        }
        return $this->render('import', [
            'model' => $model,
        ]);
    }
}
