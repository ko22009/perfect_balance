<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tarifs;

/**
 * TarifsSearch represents the model behind the search form of `app\models\Tarifs`.
 */
class TarifsSearch extends Tarifs
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tarif_num', 'tarif_id', 'city_id'], 'integer'],
            [['food_1', 'food_2', 'food_3', 'food_4', 'food_5', 'food_6'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tarifs::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tarif_id' => $this->tarif_id,
            'tarif_num' => $this->tarif_num,
            'city_id' => $this->city_id,
            'food_1' => $this->food_1,
            'food_2' => $this->food_2,
            'food_3' => $this->food_3,
            'food_4' => $this->food_4,
            'food_5' => $this->food_5,
            'food_6' => $this->food_6,
        ]);

        return $dataProvider;
    }
}
