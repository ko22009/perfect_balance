<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "slides".
 *
 * @property int $id
 * @property int $city_id
 * @property int $theme_id
 * @property string $title
 * @property string $subtitle
 * @property int $discount
 * @property string $link
 * @property string $nav_title
 * @property string $image
 */
class Slides extends \common\models\Slides
{
}