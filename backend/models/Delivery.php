<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "delivery".
 *
 * @property int $id
 * @property int $city_id
 * @property int $week_number
 * @property string $time
 */
class Delivery extends \common\models\Delivery
{
}
