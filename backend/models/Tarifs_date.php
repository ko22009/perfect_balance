<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tarifs_date".
 *
 * @property int $id
 * @property string $date
 * @property string $name
 * @property int $calories
 * @property int $proteins
 * @property int $fats
 * @property int $carbohydrates
 */
class Tarifs_date extends \common\models\Tarifs_date
{
}
