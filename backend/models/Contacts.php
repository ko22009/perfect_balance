<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contacts".
 *
 * @property int $id
 * @property int $city_id
 * @property string $number
 * @property string $time_text
 */
class Contacts extends \common\models\Contacts {}