<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tarifs".
 *
 * @property int $id
 * @property string $tarif_name
 * @property int $tarif_num
 * @property double $food_1
 * @property double $food_2
 * @property double $food_3
 * @property double $food_4
 * @property double $food_5
 * @property double $food_6
 */
class Tarifs extends \common\models\Tarifs
{
}
