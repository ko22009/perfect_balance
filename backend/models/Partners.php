<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "partners".
 *
 * @property int $id
 * @property int $city_id
 * @property string $image
 * @property string $link
 */
class Partners extends \common\models\Partners
{
}