<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tarifs_sections".
 *
 * @property int $id
 * @property string $name
 * @property string $tarifs
 */
class TarifsSections extends \common\models\TarifsSections {

}