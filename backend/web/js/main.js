$(document).ready(function () {

	var tarif = {'id': 0, 'city': 0}

	var select_tarif = $('#tarifs-tarif_id')
	var select_tarif_val = select_tarif.val()
	var data_option_tarif = {"theme": "krajee", "width": "100%", "placeholder": "Select a tarif ...", "language": "ru-RU"}
	var data_option_day = {
		"theme": "krajee",
		"width": "100%",
		"placeholder": "Select a day ...",
		"language": "ru-RU",
		allowClear: true
	}

	var select_city = $('#tarifs-city_id')
	var select_money = $('#tarifs_money')

	select_money.on('afterAddRow', function (e, row) {
		changeMoney(row)
	})

	function changeTarif(predifined = false) {
		select_tarif.empty().trigger("change")
		$.post("/tarifs/city-change", {city_id: tarif.city}, function (data) {
			if (data.length) {
				select_tarif.select2($.extend({data: data}, data_option_tarif))
				select_tarif.val(select_tarif_val)
				select_tarif.change()
			}
		})
		if (!predifined) tarif.id = 0
		else changeMoney()
	}

	/**
	 * Смена тарифом при смене городов
	 */

	select_city.change(function () {
		tarif.city = $(this).val()
		if (tarif.city) changeTarif()
	})

	/**
	 * Смена дней при смене тарифов
	 */

	select_tarif.change(function () {
		tarif.id = $(this).val()
		changeMoney()
	})

	/**
	 * Если предопределен изначально город и тариф, то подгружаем их
	 */

	var city_id = select_city.val()
	var tarif_id = select_tarif.val()
	if (city_id) {
		tarif.city = city_id
		if (tarif_id) {
			tarif.id = tarif_id
			changeTarif(true)
		}
		else changeTarif()
	}

	function changeMoney(row = false) {
		$.post("/tarifs/tarif-change", {city_id: tarif.city, tarif_id: tarif.id}, function (data) {
			var i = 0
			var days = _.map(JSON.parse(data['days']), function(value, prop) {
				return { id: prop, text: value };
			});
			if (row) {
				row.find('select').select2($.extend({data: days}, data_option_day))
			}
			else {
				var res = []
				select_money.find('select').each(function (index) {
					res[index] = $(this).val()
				})
				select_money.find('select').empty().trigger("change")

				select_money.find('select').select2($.extend({data: days}, data_option_day))

				select_money.find('select').each(function (index) {
					$(this).val(res[index]).trigger("change")
				})
			}
		})
	}

	/* tarifs_sections */
	var select_section = $('#tarifssections-city_id')
	var container_selector_tarifs = $('#tarifs_section_tarif')
	var select_section_val = select_section.val()
	if(select_section_val) changeTarifSections(select_section_val)
	select_section.change(function () {
		select_section_val = $(this).val()
		changeTarifSections(select_section_val)
	})

	function changeTarifSections(city_id, row = false) {
		$.post("/tarifs_sections/tarif", {city_id: city_id}, function (data) {
			var i = 0
			if (row) {
				row.find('select').select2($.extend({data: data}, data_option_day))
			}
			else {
				var res = []
				container_selector_tarifs.find('select').each(function (index) {
					res[index] = $(this).val()
				})
				container_selector_tarifs.find('select').empty().trigger("change")

				container_selector_tarifs.find('select').select2($.extend({data: data}, data_option_tarif))

				container_selector_tarifs.find('select').each(function (index) {
					$(this).val(res[index]).trigger("change")
				})
			}
		})
	}

	container_selector_tarifs.on('afterAddRow', function (e, row) {
		changeTarifSections(select_section_val, row)
	})

});
