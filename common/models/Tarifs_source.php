<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tarifs_source".
 *
 * @property int $id
 * @property int $city_id
 * @property string $name
 * @property int $order
 * @property string $image
 */
class Tarifs_source extends \yii\db\ActiveRecord
{
    public $imageFile;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tarifs_source';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order', 'city_id'], 'required'],
            [['order', 'city_id'], 'integer'],
            [['order', 'city_id'], 'unique', 'targetAttribute' => ['order', 'city_id']],
            [['name'], 'string', 'max' => 128],
            [['imageFile', 'days', 'safe'], 'safe'],
            [['imageFile'], 'file', 'extensions'=>'jpg, gif, png']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'order' => 'Order',
            'image' => 'image',
            'city_id' => 'City ID',
            'days' => 'Days'
        ];
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if ($this->days)
                $this->days = json_encode(array_values($this->days));
            return true;
        } else {
            return false;
        }
    }

    public function getTarifs()
    {
        return $this->hasMany(Tarifs::class, ['tarif_id' => 'id']);
    }

    public function getTarifs_sections()
    {
        return $this->hasMany(TarifsSections::class, ['city_id' => 'city_id']);
    }

    public function getCities()
    {
        return $this->hasOne(Cities::class, ['id' => 'city_id']);
    }
}
