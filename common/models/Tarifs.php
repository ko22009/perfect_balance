<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tarifs".
 *
 * @property int $id
 * @property int $tarif_id
 * @property int $tarif_num
 * @property int $city_id
 * @property double $food_1
 * @property double $food_2
 * @property double $food_3
 * @property double $food_4
 * @property double $food_5
 * @property double $food_6
 * @property string $money
 */
class Tarifs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tarifs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'tarif_id'], 'required'],
            [['tarif_num', 'tarif_id', 'city_id'], 'integer'],
            [['food_1', 'food_2', 'food_3', 'food_4', 'food_5', 'food_6'], 'number'],
            [['money'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tarif_id' => 'Tarif ID',
            'tarif_num' => 'Tarif Num',
            'city_id' => 'City ID',
            'food_1' => 'Food 1',
            'food_2' => 'Food 2',
            'food_3' => 'Food 3',
            'food_4' => 'Food 4',
            'food_5' => 'Food 5',
            'food_6' => 'Food 6',
            'money' => 'Money'
        ];
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if ($this->money)
                $this->money = json_encode(array_values($this->money), JSON_NUMERIC_CHECK);
            return true;
        } else {
            return false;
        }
    }

    public function getDays() {
        return $this->hasOne(Tarifs_source::class, ['city_id' => 'city_id']);
    }

    public function getCities()
    {
        return $this->hasOne(Cities::class, ['id' => 'city_id']);
    }

    public function getTarifs_source()
    {
        return $this->hasOne(Tarifs_source::class, ['id' => 'tarif_id']);
    }

}
