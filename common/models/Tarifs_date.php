<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tarifs_date".
 *
 * @property int $id
 * @property int $city_id
 * @property string $date
 * @property string $name
 * @property int $calories
 * @property int $proteins
 * @property int $fats
 * @property int $carbohydrates
 */
class Tarifs_date extends \yii\db\ActiveRecord {
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'tarifs_date';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['city_id'], 'required'],
            [['date'], 'safe'],
            [['calories', 'proteins', 'fats', 'carbohydrates', 'city_id'], 'integer'],
            [['name'], 'string', 'max' => 128],
            [['date', 'name', 'calories', 'proteins', 'fats', 'carbohydrates', 'city_id'], 'unique', 'targetAttribute' => ['date', 'name', 'calories', 'proteins', 'fats', 'carbohydrates', 'city_id']],
            [['file'], 'safe'],
            [['file'], 'file', 'extensions'=>'xls, xlsx']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'name' => 'Name',
            'calories' => 'Calories',
            'proteins' => 'Proteins',
            'fats' => 'Fats',
            'carbohydrates' => 'Carbohydrates',
        ];
    }
    public function getCities()
    {
        return $this->hasOne(Cities::class, ['id' => 'city_id']);
    }
}
