<?php

namespace common\models;

use common\models\Cities;
use Yii;

/**
 * This is the model class for table "reviews".
 *
 * @property int $id
 * @property int $city_id
 * @property string $name
 * @property string $description
 * @property string $terif
 * @property string $result
 * @property string $text
 * @property string $before_image
 * @property string $after_image
 */
class Reviews extends \yii\db\ActiveRecord
{
    public $imageFile1;
    public $imageFile2;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reviews';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id'], 'required'],
            [['city_id'], 'integer'],
            [['text'], 'string'],
            [['name', 'description', 'terif', 'result'], 'string', 'max' => 64],
            [['imageFile1', 'imageFile2'], 'safe'],
            [['imageFile1', 'imageFile2'], 'file', 'extensions'=>'jpg, gif, png']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'name' => 'Name',
            'description' => 'Description',
            'terif' => 'Terif',
            'result' => 'Result',
            'text' => 'Text',
            'imageFile1' => 'Фотография ДО',
            'imageFile2' => 'Фотография ПОСЛЕ'
        ];
    }
    public function getCities()
    {
        return $this->hasOne(Cities::class, ['id' => 'city_id']);
    }
}
