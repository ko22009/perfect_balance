<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "crew".
 *
 * @property int $id
 * @property int $city_id
 * @property string $name
 * @property string $position
 * @property string $quote
 * @property string $image
 */
class Crew extends \yii\db\ActiveRecord
{
    public $imageFile;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'crew';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id'], 'required'],
            [['city_id'], 'integer'],
            [['quote'], 'string'],
            [['name', 'position'], 'string', 'max' => 64],
            [['imageFile'], 'safe'],
            [['imageFile'], 'file', 'extensions'=>'jpg, gif, png']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'name' => 'Name',
            'position' => 'Position',
            'quote' => 'Quote',
            'image' => 'Image',
        ];
    }
    public function getCities()
    {
        return $this->hasOne(Cities::class, ['id' => 'city_id']);
    }
}
