<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "kitchens".
 *
 * @property int $id
 * @property int $city_id
 * @property string $link
 * @property string $image
 */
class Kitchens extends \yii\db\ActiveRecord
{
    public $imageFile;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kitchens';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id'], 'required'],
            [['city_id'], 'integer'],
            [['image'], 'string'],
            [['imageFile'], 'safe'],
            [['imageFile'], 'file', 'extensions'=>'jpg, gif, png']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'image' => 'Image',
        ];
    }
    public function getCities()
    {
        return $this->hasOne(Cities::class, ['id' => 'city_id']);
    }
}
