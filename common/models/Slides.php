<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "slides".
 *
 * @property int $id
 * @property int $city_id
 * @property int $theme_id
 * @property string $title
 * @property string $subtitle
 * @property string $discount
 * @property int $link
 * @property string $nav_title
 * @property string $image
 */
class Slides extends \yii\db\ActiveRecord
{
    public $imageFile;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slides';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'theme_id'], 'required'],
            [['city_id', 'theme_id', 'link'], 'integer'],
            [['title', 'subtitle', 'nav_title', 'discount'], 'string'],
            [['imageFile'], 'safe'],
            [['imageFile'], 'file', 'extensions'=>'jpg, gif, png']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'theme_id' => 'Theme ID',
            'title' => 'Title',
            'subtitle' => 'Subtitle',
            'discount' => 'Discount',
            'link' => 'Link',
            'nav_title' => 'Nav Title',
            'image' => 'Image',
        ];
    }
    public function getCities()
    {
        return $this->hasOne(Cities::class, ['id' => 'city_id']);
    }
    public function getTheme()
    {
        return $this->hasOne(Type_themes::class, ['id' => 'theme_id']);
    }
}
