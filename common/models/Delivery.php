<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "delivery".
 *
 * @property int $id
 * @property int $city_id
 * @property int $week_number
 * @property string $time
 * @property string $time2
 */
class Delivery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'delivery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id'], 'required'],
            [['city_id', 'week_number', 'week_number2'], 'integer'],
            [['time', 'time2', 'time3'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'week_number' => 'Week Number',
            'time' => 'Time',
            'time2' => 'Time',
            'week_number2' => 'Сделать заказ до дата',
            'time3' => 'Сделать заказ до время'
        ];
    }
    public function getCities()
    {
        return $this->hasOne(Cities::class, ['id' => 'city_id']);
    }
}
