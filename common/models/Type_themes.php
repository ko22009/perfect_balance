<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "type_themes".
 *
 * @property int $id
 * @property string $name
 * @property int $indicator
 */
class Type_themes extends \yii\db\ActiveRecord
{
    const COLOR_WHITE = 1;
    const COLOR_BLACK = 2;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'type_themes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 64],
            [['indicator'], 'required'],
            [['indicator'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'indicator' => 'Indicator'
        ];
    }
}
