<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "contacts".
 *
 * @property int $id
 * @property int $city_id
 * @property string $number
 * @property string $time_text
 */
class Contacts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contacts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id'], 'required'],
            [['city_id'], 'integer'],
            [['number'], 'string', 'max' => 18],
            [['time_text'], 'string', 'max' => 64],
            [['vk'], 'string', 'max' => 128],
            [['instagram'], 'string', 'max' => 128],
            [['fb'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'number' => 'Number',
            'time_text' => 'Time Text',
            'vk' => 'VK',
            'instagram' => 'instagram',
            'fb' => 'FB'
        ];
    }

    public function getCities()
    {
        return $this->hasOne(Cities::class, ['id' => 'city_id']);
    }
}
