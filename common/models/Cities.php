<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cities".
 *
 * @property int $id
 * @property string $name
 * @property string $domen
 * @property float $latitude
 * @property float $longitude
 */
class Cities extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'cities';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'domen', 'google_id', 'yandex_id', 'fb_pixel_id', 'vk_pixel_id', 'vk_pixel_view', 'vk_pixel_madeOrder', 'vk_pixel_call'], 'string', 'max' => 64],
            [['placeholder_destination'], 'string', 'max' => 128],
            [['user_id_amo_crm'], 'string', 'max' => 12],
            [['latitude', 'longitude'], 'safe'],
            [['down'], 'boolean']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'domen' => 'Domen',
            'down' => 'Down',
            'user_id_amo_crm' => 'User ID AMO-CRM',
            'google_id' => 'google_id',
            'yandex_id' => 'yandex_id',
            'fb_pixel_id' => 'fb_pixel_id',
            'vk_pixel_id' => 'vk_pixel_id',
            'vk_pixel_view' => 'vk_pixel_view',
            'vk_pixel_madeOrder' => 'vk_pixel_madeOrder',
            'vk_pixel_call' =>  'vk_pixel_call',
            'placeholder_destination' => 'placeholder_destination',
        ];
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if ($this->name) {
                $url = 'http://geocode-maps.yandex.ru/1.x/?format=json&geocode=' . $this->name;
                $res = json_decode(file_get_contents($url));
                $lats = $res->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;
                $lats = explode(' ', $lats);
                $this->longitude = $lats[0];
                $this->latitude = $lats[1];
            }
            return true;
        } else {
            return false;
        }
    }
}
