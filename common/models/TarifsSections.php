<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tarifs_sections".
 *
 * @property int $id
 * @property int $city_id
 * @property string $name
 * @property string $tarifs
 */
class TarifsSections extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tarifs_sections';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tarifs'], 'safe'],
            [['name'], 'string', 'max' => 64],
            [['city_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'tarifs' => 'Tarifs',
            'city_id' => 'City ID'
        ];
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if ($this->tarifs)
                $this->tarifs = json_encode($this->tarifs['tarif_id']);
            return true;
        } else {
            return false;
        }
    }
}
